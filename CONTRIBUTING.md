# Contribuer à e-comBox

N'hésitez pas à contribuer au projet e-comBox et à le rendre meilleur avec nous ! :rocket:

- [Question ou Problème?](#question)
- [Tickets et Bugs](#issue)
- [Demande de fonctionnalité](#feature)
- [Consignes de soumission](#submit-mr)
- [Règles de developpemnt](#rules)
- [Consignes de message de commit](#commit)

## <a name="question"></a> Une question ou un problème ?

Si vous avez une question générale sur le fonctionnement d'e-comBox ou un problème lors de son utilisation, merci d'utiliser le [support prévu à cet effet](https://llb.ac-corse.fr/ecomboxsupport/my_view_page.php) et de ne pas ouvrir de ticket sur GitLab.

Il vous sera demandé de créer un compte afin d'avoir un suivi de votre question et que tous les utilisateurs de l'e-comBox puisse visualiser les questions déjà posées.

## <a name="issue"></a> Vous avez relevé un bug ?

Si vous relevez un bug ou dans le code source ou une erreur dans la documentation, vous pouvez nous aider [en ouvrant un ticket (issue)](#submit-issue) sur notre [dépôt GitLab](). Pour que l'équipe puisse rapidement résoudre le bug, décrire les étapes de reproduction du bug est très important. Les captures d'écran et les logs sont également très utiles.

Veuillez suivre ces étapes de contrôle avant de soumettre :

- Si vous avez une question sur l'utilisation et/ou l'installation d'e-comBox, ouvrez un ticket sur le [support](https://llb.ac-corse.fr/ecomboxsupport/my_view_page.php).

- Vous devez décrire clairement les étapes nécessaires pour reproduire le problème que vous rencontrez. Bien que nous aimerions aider les utilisateurs autant que possible, diagnostiquer les problèmes sans étapes de reproduction claires prend énormément de temps.

- La liste des problèmes de ce dépôt est exclusivement destinée aux rapports de bugs et aux demandes de fonctionnalités. Les problèmes non conformes seront fermés immédiatement.

- Les problèmes sans étapes claires à reproduire ne seront pas triés. Si un problème est étiqueté avec "need informations" et ne reçoit aucune autre réponse de l'auteur du problème pendant plus de 5 jours, il sera fermé.

- Si vous pensez avoir trouvé un bug ou avez une nouvelle idée de fonctionnalité, veuillez commencer par vous assurer qu'il n'a pas déjà été [signalé][problème]. Vous pouvez rechercher parmi les tickets existants pour voir s'il y en a un similaire signalé. Pensez à vérifier les tickets fermés car ils peuvent avoir été fermés avec une solution.

- Ensuite, [créez un nouveau ticket](#submit-issue) qui explique en détail le problème. Veuillez compléter le formulaire de bug avant de soumettre le problème.

## <a name="feature"></a> Vous voulez une nouvelle fontionnalité ?

Vous pouvez _demander_ une nouvelle fonctionnalité [en ouvrant un ticket](#submit-issue) sur notre [dépôt GitLab][github]. Si vous souhaitez _implémenter_ une nouvelle fonctionnalité, veuillez soumettre un ticket avec une proposition de votre travail d'abord, pour être sûr que nous pourrons l'utiliser.

Lorsque vous désirez soumettre une nouvelle fonctionnalité, veuillez préciser de quel type de changement il s'agit :

- Pour une **fonctionnalité majeure**, ouvrez d'abord un ticket et décrivez votre proposition afin qu'elle puisse être étudiée. Cela nous permettra également de mieux coordonner nos efforts, d'éviter les doubles emplois, et de vous aider à concevoir le changement afin qu'il soit accepté avec succès dans le projet.
- Les **petites fonctionnalités** peuvent être implémentées et directement [soumises en tant que merge request](#submit-mr).

### <a name="submit-issue"></a> Ouvrir un ticket

Avant d'ouvrir un ticket, recherchez dans les archives, peut-être y-a-t-il déjà une réponse à votre question.

Si votre problème semble être un bug et n'a pas été signalé, ouvrez un nouveau ticket avec le modèle "bug". Assurez-vous d'avoir complété le formulaire de ticket avant de le soumettre.

Vous pouvez déposer de nouveaux tickets en fournissant les informations [ici][nouveau_numéro].

### <a name="submit-mr"></a> Soumettre une merge request (MR)

Avant de soumettre votre merge request (MR), tenez compte des directives suivantes :

- Recherchez [GitLab][https://forge.apps.education.fr/e-combox/e-combox_webapp/-/merge_requests] pour une MR ouverte ou fermée qui se rapporte à votre soumission.
- Effectuez vos modifications dans une nouvelle branche git :

  ```shell
  git checkout -b my-fix-branch master
  ```

- Lisez [documentation de développement][dev-doc].
- Créez votre patch, **en incluant les tests appropriés**.
- Suivez nos [règles de développement](#rules).
- Testez vos modifications sur les différents navigateaurs existants ainsi que sur mobile.
- Validez vos modifications à l'aide d'un message de validation descriptif qui respecte nos [conventions de messages de validation](#commit). Le respect de ces conventions est nécessaire car les notes de version sont automatiquement générées à partir de ces messages.

  ```shell
  git commit -a
  ```

  Remarque : l'option de ligne de commande "-a" de validation, "ajoutera" et "supprimera" automatiquement les fichiers modifiés.

- Poussez votre branche vers GitLab :

  ```shell
  git push my-fork my-fix-branch
  ```

- Sur GitLab, envoyez une merge request à `e-comBox_webapp:master`.
- Si nous proposons des modifications, alors :

  - Effectuez les mises à jour nécessaires.
  - Réexécutez `npm run lint` pour vous assurer que les tests réussissent toujours, que linter & build n'a pas d'erreurs.
  - Rebasez votre branche et forcez le push vers votre dépôt GitLab (cela mettra à jour votre Merge Request) :

    ```shell
    git rebase master -i
    git push -f
    ```

Nous vous remercions pour vos contributions !

#### Une fois que votre Merge Request a été acceptée

Une fois que vos modifications on été fusionnées, vous pouvez supprimer sans risques votre branche et récupérer les changement du dépôt principal (upstream) :

- Supprimez la branche distante sur GitLab via l'interface utilisateur Web GiLab ou votre shell local :

  ```shell
  git push my-fork --delete my-fix-branch
  ```

- Positionnez-vous sur la branche master :

  ```shell
  git checkout master -f
  ```

- Supprimer la branche locale :

  ```shell
  git branch -D my-fix-branch
  ```

- Mettez à jour votre branche master avec les dernières modifications :

  ```shell
  git pull --ff upstream master
  ```

## <a name="rules"></a> Règles de développement

Pour assurer la cohérence dans l'ensemble du code source, gardez ces règles à l'esprit lorsque vous travaillez :

- Toutes les fonctionnalités ou corrections de bugs **doivent être testées** par une ou plusieurs spécifications (tests unitaires).
- Toutes les méthodes **doivent être documentées** en respectant la notation JsDoc.

## <a name="commit"></a> Consignes relatives aux messages de validation

Nous avons des règles très précises sur la façon dont nos messages de validation git peuvent être formatés. Cela conduit à **des messages plus lisibles** faciles à suivre lorsque vous parcourez l'**historique du projet**. Mais aussi,
nous utilisons les messages de validation git pour **générer le journal des modifications e-comBox**.

### Format du message de validation

Chaque message de validation se compose d'un **en-tête**, d'un **corps** et d'un **pied de page**. L'en-tête a un format spécial qui inclut un **type**, une **portée** et un **sujet** :

```
[type]: <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

L'**en-tête** est obligatoire.

Toute ligne du message de validation ne doit pas dépasser 100 caractères. Cela permet au message d'être plus simple
à lire sur GitLab ainsi que dans les divers outils git.

### Revert

Si le commit annule un commit précédent, il doit commencer par `[revert]`, suivi de l'en-tête du commit annulé. Dans le corps, il devrait dire : `This reverts commit <hash>.`, où le hachage est le SHA du commit étant annulé.

### Type

Avec un type parmi :

- **\[init\]** : pour le dépôt initial (premier import des fichiers)
- **\[feat]** : pour l’ajout d’une nouvelle fonctionnalité
- **\[update]** : pour la modification d’une fonctionnalité ou du fonctionnement de l’application
- **\[fix]** : pour la correction d’un bug
- **\[doc]** : pour l’ajout ou la mise à jour de documentation et/ou commentaires
- **\[test]** : pour l’ajout ou la modification de tests
- **\[refactor]** : dans le cas d’un changement de code qui ne modifie pas le fonctionnement
- **\[perf]** : pour une modification qui améliore les performances
- **\[build]** : pour un changement qui affecte le système de build ou des dépendances externes
- **\[style]** : changement qui n’apporte aucune altération fonctionnelle ou sémantique (indentation, mise en forme, ajout d’espace, renommage de variables, …)
- **\[ci]** : lorsque les modifications concernent les fichiers et scripts d’intégration continue
- **\[del]** : pour la suppression d’une fonctionnalité ou d’un fichier ou dossier
- **\[misc]** : quand aucun des types précédents ne correspond au commit

### Subject

Le sujet contient une description succincte de la modification :

- utilisez l'impératif, le présent : "change" et non "a été changé" ni "a changé"
- ne pas mettre la première lettre en majuscule
- pas de point (.) à la fin
- préférez utiliser l'anglais

### Body

Optionnel. Tout comme dans le **sujet**, utilisez l'impératif, le présent.
Le corps doit inclure la motivation de la modification et la mettre en contraste avec le comportement antérieur.

### Footer

Optionnel. Le pied de page doit contenir toutes les informations sur les **Breaking Changes** et est également l'endroit où référencez les issues GitLab que ce commit **ferme**.

[dev-doc]: https://e-combox.gitlab.io/e-combox_webapp/
[gitlab]: https://forge.apps.education.fr/e-combox/e-combox_webapp
[issues]: https://forge.apps.education.fr/e-combox/e-combox_webapp/-/issues
[new_issue]: https://forge.apps.education.fr/e-combox/e-combox_webapp/-/issues/new
[mr]: https://forge.apps.education.fr/e-combox/e-combox_webapp/-/merge_requests
[workflow]: https://docs.google.com/document/d/1RFCah8opd8AvEq4DKx2RTMU_ENwr026nerbbK3p9HWw/edit?usp=sharing
