**Voir la [feuille de route](https://forge.apps.education.fr/e-combox/e-combox_webapp/-/blob/4.3/ROADMAP.md).**

# [4.3.0] (2024-03-10)

### Fonctionnalités

* **Réinitialiser un site** : ajout d'un bouton sur la carte d'un site permettant de le restaurer à son état initial
* Corrections et améliorations diverses

### Mise à jour des scripts d'installation
* **Voir le [CHANGELOG des scripts](https://forge.apps.education.fr/e-combox/e-combox_scriptslinux/-/blob/4.3/CHANGELOG.md?ref_type=heads).**

# [4.2.3] (2023-02-12)

### Fonctionnalités

* Mise à jour de Portainer
* Corrections et améliorations diverses

# [4.2.0] (2023-07-31)

### Fonctionnalités

* **Authentification** : Ajout de l'authentification OAuth
* Corrections et améliorations diverses

# [4.1.2] (2023-02-21)

### Fonctionnalités

* **Sites** : visualisation des identifiants de connexion par défaut directement à partir de l'interface e-comBox.
* **Partager ses propres modèles de sites** : possibilité de partager des modèles de sites directement à partir de l'interface e-comBox après avoir créé un compte sur la plateforme Docker Hub.
* **Modèles de sites** : possibilité de modifier le nom d'un modèle existant.
* **Actions sur les sites** : possibilité de slectionner les sites désirés pour démarrer/stopper/supprimer ces derniers plutôt que de tous les sélectionner.
* **Annonces** : possibilité pour l'équipe e-comBox d'ajouter des annonces.
* **Design** : améliorations graphiques diverses.
* **Apllications métiers** : ajout de la base Odoo Sweety Bio

### Sécurité et performance

* Mise à jour vers Angular 13.
* Affichage sur le dashnoard de la version des scripts utilisées pour l'installation.
* Optimisations de stabilité et de performance diverses.

# [4.0.0] (2022-11-20)

### Fonctionnalités

* **Reverse Proxy** : possibilité de passer par un Reverse Proxy de manière à ne pas être obligé d'exposer des ports (mis à part le port d'accès à l'interface) sur le serveur et à ne dédier qu'une seule adresse IP publique pour l'ensemble des instances.
* **Authentification** : module permettant l'authentification des professeurs sur l'interface via un système d'authentification multi-utilisateurs interne à l'e-comBox.
* **SFTP** : accès au SFTP via le Web (plus besoin de client sftp et de ports supplémentaires ouverts).
* **Portainer** : synchronisation automatique de l'application avec le mot de passe défini par l'administrateur dans Portainer.

### Sécurité

* **HTTPS** : intégration du protocole HTTPS au niveau des sites et de toute l'application (Portainer et e-comBox)

<a name="3.0.4"></a>
# [3.0.4] (2022-02-07)

### Fonctionnalités

* Ajout du modèle de site BdDev pour Kanboard

<a name="3.0.3"></a>
# [3.0.3] (2022-01-07)

### Fonctionnalités

* Ajout du modèle de site AdA pour Odoo

<a name="3.0.2"></a>
# [3.0.2] (2021-12-29)

### Correctifs

* **Tableau de bord :** correction de la méthode de vérification de la présence ou non d'une mise à jour

### Fonctionnalités

* Ajout des modèles de sites Generik et Pépinières pour Odoo

<a name="3.0.1"></a>
# [3.0.1] (2021-11-24)


### Correctifs

* **Démarrer tous les sites :** en cas de ressources insuffisantes, le démarrage de tous les sites simultanément posaient problème.
* **Duplication de site :** correction d'un bug qui empêchait la création d'un site Mautic à partir d'un modèle de site personnalisé.
* **Duplication de site :** correction d'un un bug qui touchait toutes les versions de Odoo qui empêchait la création d'un site Odoo à partir d'un modèle de site personnalisé dès lors que le "master password" était modifié.


### Fonctionnalités

* **Instance de l'e-comBox :** affichage de l'instance de l'e-comBox sur le tableau de bord.
* **Mise à jour disponible :** affichage d'un message sur le tableau de bord lorsqu'une nouvelle mise à jour de l'e-comBox est disponible.
* **Changelog :** affichage d'un lien vers les changelogs sur le tableau de bord.



# [3.0.0] (2021-07-06)


### Fonctionnalités

* Réécriture du code (meilleures adaptabilité et fluidité).
* Possibilité de créer "n" sites en même temps.
* Sauvegarde/restauration globale (en demandant un chemin à l’utilisateur).
* Possibilité pour les professeurs de sauvegarder leur serveur à un instant T et en faire une image afin de pouvoir créer des serveurs à partir de cette dernière.
* Gestion des images personnalisées par les professeurs.
* Odoo : ajout de la version 14 et des bases personnalisées "Surplomb" et "Primeur".
* kanboard : mise à jour vers la version 1.2.21, intégration des plugins et possibilité de les installer via l'interface.



# [2.0.0] (2020-12-18)


### Sécurisation des sites et de l'application

* Accès aux sites à partir d’une URL (par exemple : http://adresseIP:troisièmePort/nom_site) en masquant la redirection vers les différents ports via un reverse proxy : seuls 3 ports plus les ports pour le SFTP (si ce dernier est utilisé) allant de 2200-2299 doivent être ouverts via un pare-feu. 
* Choix possible des ports utilisés par l’application. Les ports 8800 (pour l'accès au sites), 8880 (pour l'accès à l'administration avancée) et 8888 (pour l'accès à l'application) sont proposés par défaut.
* Les mots de passe n'apparaissent plus dans les Docker Compose.
* Accès à distance à l'interface d'e-comBox avec possibilité de créer un nom d'utilisateur et un mot de passe.
* Modification possible du mot de passe du compte "admin" de Portainer (administration avancée).
* Avertissement via un bandeau au niveau de l'application lorsqu'une mise à jour est disponible.

### Mis à jour de la version des sites
* Prestashop : passage de la version 1.7.5.1 à la version 1.7.6.5 pour Prestashop-art et 1.7.6.6 pour Prestashop-vierge.
* Wordpress : passage de la version 5.2 à la version 5.4.2.
* Mautic : passage de la version 2.15.1 à la version 3.
* Odoo : ajout de la version 13.

### Actions sur les sites
* Langue française par défaut pour Suite CRM et Humhub.
* Intégration des modules utiles sur Odoo.
* Plus de timeout de nginx pour kanboard.
