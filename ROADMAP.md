# V5

## 5.0.0

**Date de sortie prévue** : février 2024

### Fonctionnalités

- **Grouper des sites** : possibilité pour l'enseignant de créer des groupes afin de classer plus facilement les sites.
- **Compte prof** : possibilité de créer des comptes élèves.
- **Accès aux sites** : possiblité pour l'enseignant de choisir si un site doit être publié sur Internet ou pas.
- **Modification du nommage des sites** : possibilité de choisir les noms pour les sites lorsque l'on en crée plusieurs à partir d'une même configuration.

# V4
C'est la version qui va permettre de déployer l'e-comBox à grande échelle dans les académies et dans les régions.

## 4.2.0

**Date de sortie prévue** : mai 2023

### Fonctionnalités

- **Authentification** : module permettant l'authentification des professeurs sur l'interface e-comBox via OAuth.
- **Ajout d'un modèle de site** : ajout d'un nouveau modèle WordPress avec le thème ...
- **Réinitialiser un site existant**

### Divers

- **Mise à jour des applications**
- **Reverse-Proxy e-comBox** : configuration de nginx permettant d'accèder à différents services tiers à partir de l'application e-comBox (suppression de l'utilisation de cors-anywhere).
- **Améliorations de fonctionnement** : récupération automatique de la nouvelle image si elle a été modifiée sur le dépôt central. 
- **Optimisations diverses**
- **Améliorations liées à la sécurité des images Docker** 

## 4.1.0

**Date de sortie prévue** : février 2023

### Fonctionnalités

- **Partager ses propres modèles de sites** : le partage de modèles de sites sera possible directement à partir de l'interface e-comBox après avoir créé un compte sur la plateforme Docker Hub.
- **Récupérer un modèle de site créé par un utilisateur de la communauté** : il sera désormais possible, à partir du nom complet du modèle, de créer un site à partir d'un modèle partagé par un utilisateur de la communauté e-comBox.
- **Actions sur les sites** : possibilité de réaliser une action (supprimer, stopper, démarrer) sur plusieurs sites en les sélectionnant. 
- **Accès aux sites** : accès aux identifiants de connexion des différents sites driectement à partir de l'interface d'e-comBox.

### Divers

- **Mise à jour des applications**
- **Mise à niveau vers Angular 13**
- **Installation facilitée** : installation de l'e-comBox sans intervention de l'administrateur.
- **Déploiement automatisé** : exemple de script et de documentation pour un déploiement via Ansible.
- **Certificats** : gestion optimisée et facilitée avec un script permettant de générer un certificat avec letsEncrypt.
- **Performances** : amélioration de la fluidité d'utilisation de l'interface e-comBox.

## 4.0.0

**Date de sortie** : 20 novembre 2022

### Fonctionnalités

- **Reverse Proxy** : possibilité de passer par un Reverse Proxy de manière à ne pas être obligé d'exposer des ports (mis à part le port d'accès à l'interface) sur le serveur et à ne dédier qu'une seule adresse IP publique pour l'ensemble des instances.
- **Authentification** : module permettant l'authentification des professeurs sur l'interface via un système d'authentification multi-utilisateurs interne à l'e-comBox.
- **SFTP** : accès au SFTP via le Web (plus besoin de client sftp et de ports supplémentaires ouverts) 
- **Portainer** : synchronisation automatique de l'application avec le mot de passe défini par l'administrateur dans Portainer.
- **Sites** : visualisation des identifiants de connexion de chaque type de site directement à partir de l'application e-comBox.

### Sécurisation

- **HTTPS** : Intégration du protocole HTTPS au niveau des sites et de toute l'application (Portainer et e-comBox)


# V3

## Pour toutes les versions

* **kanboard** : mise à jour vers la version 1.2.21, intégration des plugins et possibilité de les installer via l'interface. 

## 3.2.0

### Fonctionnalités

* **Partager ses propres modèles de sites** : le partage de modèles de sites sera possible directement à partir de l'interface e-comBox après avoir créé un compte sur la plateforme Docker Hub
* **Récupérer un modèle de site créé par un utilisateur de la communauté** : il sera désormais possible, à partir du nom complet du modèle, de créer un site à partir d'un modèle partagé par un utilisateur de la communauté e-comBox.


## 3.1.0

- La **v3.1** est prévue pour le **28/02/2022**.


**Lien vers les "issues"** : https://forge.aeif.fr/groups/e-combox/-/issues


### Sécurisation

* Intégration du protocole HTTPS au niveau des sites (**V3.1**).


### Bug

* Fix version in the footer


### Sites

* **Tous les types de site** : mise à jour des versions.
* **Général** : sauvegarde/restauration d'un seul site (en demandant un chemin à l’utilisateur).
* **Prestashop** : intégration d'un module tchat sur Prestashop.


### Fonctionnalités

* **Gérer vos modèles de site** : possibilité de renommer les images personnalisées par les profs (**V3.1**).
* **Sauvegarde et restauration d'un seul site.
* **En bas du menu de gauche** (sous Aide) : Intégration de CGU personnalisables (**V3.1**).
* **Mise à jour sur Linux** : automatiser les mises à jour (aucune action nécessaire pour l'administrateur sauf à lancer la mise à jour)

### Divers

* Rendre l'application un peu plus "responsive".
* Automatiser l'ajout des images personnalisées.
* Automatiser le partage d'un modèle de site créé avec l'e-comBox.
