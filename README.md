# e-comBox_webapp

## Objectif de l'application

L’application e-comBox permet d'installer le plus simplement possible, au sein d'un réseau d'établissement, sur un portable personnel ou chez un hébergeur en ligne (serveur OVH par exemple), plusieurs instances de différentes applications (prestashop, wordpress, mautic, odoo, kanboard, etc) sous forme de conteneurs docker : les serveurs pourront être créés à la demande en tant que de besoin. L’idée est ici de répondre aux besoins pédagogiques les plus étendus des enseignants tout en réduisant les contraintes techniques au minimum.

L’application e-comBox est elle-même une application _web_ “dockérisée” installable sur n’importe quel système d’exploitation (Windows 10, Linux et MacOS) qui permet de lancer et gérer le ou les conteneurs dans lesquels le service est installé.

Pour en savoir plus : https://llb.ac-corse.fr/mw/


## Documentation

Pour accéder à la documentation :  
> https://e-combox.gitlab.io/e-combox_webapp/

La documentation est mise à jour automatiquement à chaque push

## Development Setup

### Prerequisites

- Install Node.js version 14.x.x wich includes Node Package Manager
- Server instance with a complete e-comBox solution installed (See https://wiki-ecombox.btssio.corsica/index.php/Installation_sur_Linux_-_v4)

### Getting started

``` 
git clone -b dev  https://forge.aeif.fr/e-combox/e-combox_webapp.git e-combox
cd e-combox
npm i
npm start
```

> Start script is configured to launch the project on https://localhost:4200

Configure Portainer API URL with your own instance of e-comBox. In `src/app/shared/api/variables.ts`, change API_URL value with the IP address of your development server :
```
export const API_URL: string = "https://localhost:4200/portainer/api";
``` 

Then, in `proxy-config.json` file, change the host value with your own instance of Portainer :
```
"target" : "https://172.31.40.102:8800/"
``` 




