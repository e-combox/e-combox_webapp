FROM docker.io/nginx
RUN apt-get clean && apt-get update && apt-get install --no-install-recommends -y vim apache2-utils openssl \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/lib/apt/lists/* \
    && rm /etc/cron.daily/*
RUN mkdir /etc/ecombox-conf /etc/ssl/ecombox \ 
    && openssl req -new -subj "/C=FR/ST=France/L=Corse/O=Reseaucerta/CN=localhost" -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out /etc/ssl/ecombox/ecombox.crt -keyout /etc/ssl/ecombox/ecombox.key \
    && chmod 400 /etc/ssl/ecombox/ecombox.key  
COPY ./dist/ /usr/share/nginx/html/
COPY default.conf /etc/nginx/conf.d/
RUN chown -R nginx.nginx /usr/share/nginx/html/*