## Description

(Résumé l'incident)

## Étapes de reproductibilité

(Comment reproduire le bug - Très important)

## Projet d'exemple

(Si possible, créer un exemple de projet qui présente le problème, et créer un lien vers celui-ci ici dans le rapport de bug.)

## Quel est le comportement actuel du bug ?

(Que se passe-t-il réellement)

## Quel est le comportement correct attendu ?

(Ce que l'on doit voir à la place)

## Logs et/ou captures d'écran pertinents

(Coller tous les logs pertinents - Utiliser des blocs de code (`` ``) pour formater la sortie de la console, les logs et le code)

## Corrections possibles

(Si cela est possible, créer un lien vers la ligne de code qui pourrait être responsable du problème)
