/* eslint-disable require-jsdoc */

import { Component } from "@angular/core";
import { NbIconLibraries } from "@nebular/theme";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "./../environments/environment";

@Component({
  selector: "ngx-app",
  template: "<router-outlet></router-outlet>",
})
export class AppComponent {
  private libsFAicons: string[] = ["fa", "fas", "fad", "fal", "far"];

  /**
   * Constructor
   * @param {NbIconLibraries} iconLibraries
   * @param {TranslateService} translate
   */
  constructor(
    private iconLibraries: NbIconLibraries,
    translate: TranslateService
  ) {
    if (environment.production) {
      // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
      window.console.log = () => {
        // do nothing
      };
    }

    // Add font awesome icons to the ngx-admin theme
    this.libsFAicons.forEach((name) =>
      this.iconLibraries.registerFontPack(name, {
        packClass: name,
        iconClassPrefix: "fa",
      })
    );

    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang("fr");

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use("fr");
  }
}
