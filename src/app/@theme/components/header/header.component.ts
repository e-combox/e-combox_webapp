import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  NbMediaBreakpointsService,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from "@nebular/theme";

import { LayoutService } from "../../../@core/utils";
import { map, takeUntil, mergeMap } from "rxjs/operators";
import { empty, Subject } from "rxjs";
import { Router } from "@angular/router";
import { NbAuthService } from "@nebular/auth";
import { AuthService } from "../../../ecombox/shared/api/auth.service";
import { StackStore } from "../../../ecombox/shared/store/stack.store";
import { LoaderService } from "../../../ecombox/shared/services/loader.service";
import { HttpClient } from "@angular/common/http";
import { API_URL } from "../../../ecombox/shared/api/variables";
import { NbAuthJWTToken } from "@nebular/auth";

@Component({
  selector: "ngx-header",
  styleUrls: ["./header.component.scss", "./loading.css"],
  templateUrl: "./header.component.html",
})
export class HeaderComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;
  loading: boolean = true;
  internalAuth: boolean;
  logoutUrl: string;

  themes = [
    {
      value: "default",
      name: "Light",
    },
    {
      value: "dark",
      name: "Dark",
    },
    {
      value: "cosmic",
      name: "Cosmic",
    },
    {
      value: "corporate",
      name: "Corporate",
    },
  ];

  currentTheme = "corporate";

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private layoutService: LayoutService,
    private breakpointService: NbMediaBreakpointsService,
    private router: Router,
    public loaderService: LoaderService,
    private auth: NbAuthService,
    public request: AuthService,
    private store: StackStore,
    public httpClient: HttpClient
  ) { }

  ngOnInit() {
    this.changeTheme("default");

    this.currentTheme = this.themeService.currentTheme;

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService
      .onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (isLessThanXl: boolean) => (this.userPictureOnly = isLessThanXl)
      );

    this.auth.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        if (token.getOwnerStrategyName() == "username") {
          this.internalAuth = true;
        } else {
          this.internalAuth = false;
        }
      }
    });

    this.httpClient.get(`${API_URL}/settings/public`).subscribe(
      (data) => {
        if (data["OAuthLoginURI"]) {
          let client_id = data["OAuthLoginURI"];
          const begin = client_id.indexOf("client_id=") + 10;
          const uri = client_id.slice(begin);
          const end = uri.indexOf("&");
          if (end == -1) {
            client_id = uri;
          } else {
            client_id = uri.slice(0, end);
          }
          this.logoutUrl = `${data["OAuthLogoutURI"].slice(0, -10)}app/auth/login`;
        }
      }
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, "menu-sidebar");
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  navigateHelp() {
    this.router.navigate(["ecombox/help"]);
  }

  profile() {
    this.router.navigate(["ecombox/profile"]);
  }

  logout() {
    this.auth.logout("username").subscribe(
      (result) => {
        if (result.isSuccess) {
          this.store.unloadData();
          if (this.logoutUrl && !this.internalAuth) {
            window.location.assign(this.logoutUrl);
          } else {
            this.router.navigate([result.getRedirect()]);
          }
        }
      }
    );
  }
}
