import { Component } from "@angular/core";
import { environment } from "../../../../environments/environment";

@Component({
  selector: "ngx-footer",
  styleUrls: ["./footer.component.scss"],
  templateUrl: "./footer.component.html",
})
export class FooterComponent {
  public devMode: string = "";
  public year: number;

  constructor() {
    console.log(environment.production);
    if (!environment.production) {
      this.devMode = "Version en développement";
      const d = new Date();
      this.year = d.getFullYear();
    } else {
      this.devMode = environment.ecomboxVersion;
      this.year = 2024;
    }
  }
}
