import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbInputModule,
  NbMenuModule,
  NbAccordionModule
} from "@nebular/theme";
import { ThemeModule } from "../@theme/theme.module";
import { EcomboxRoutingModule } from "./ecombox-routing.module";
import { MiscellaneousModule } from "./miscellaneous/miscellaneous.module";
import { SharedModule } from "./shared/shared.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { ImagesModule } from "./images/images.module";
import { RedirectGuard } from "./redirect-guard";

import { EcomboxComponent } from "./ecombox.component";
import { HelpComponent } from "./help/help.component";
import { PolicyModule } from "./policies/policies.module";
import { FormsModule } from "@angular/forms";
import { NbAuthModule } from "@nebular/auth";
import { ProfileComponent } from "./profile/profile.component";
import { InformationComponent } from './information/information.component';
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [EcomboxComponent, HelpComponent, ProfileComponent, InformationComponent],
  imports: [
    CommonModule,
    NbCardModule,
    EcomboxRoutingModule,
    MiscellaneousModule,
    NbMenuModule,
    ThemeModule,
    SharedModule,
    DashboardModule,
    ImagesModule,
    PolicyModule,
    FormsModule,
    NbAlertModule,
    NbInputModule,
    NbAuthModule,
    NbButtonModule,
    NbAccordionModule,
    TranslateModule.forChild({
      extend: true,
    }),
  ],
  providers: [RedirectGuard],
})
export class EcomboxModule { }
