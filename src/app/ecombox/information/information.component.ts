import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { IMAGE } from '../shared/api/variables';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'ecx-information',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnInit {

  /**
   * 
   * @param {translate} undefined 
   */
  constructor(public translate: TranslateService) { }

  /**
   * Can generates automatically the PDF in an other tab and go back on the previous page.
   */
  ngOnInit(): void {
    // this.export();
    // history.back();
  }

  /**
   * Generates the PDF document with default login credentials.
   */
  export(): void {
    const docDefinition = {
      info: {
        title: "Identifiants de connexion",
        author: "e-comBox",
      },
      content: [
        {
          width: 50,
          height: 50,
          image: IMAGE,
        },
        {
          text: "Identifiants d'accès aux différentes applications d'e-comBox  \n\n\n",
          bold: true,
          fontSize: 15,
          alignment: "center",
        },
        {
          table: {
            widths: [220, 280],
            body: [
              [{ text: 'Application', bold: true }, { text: "Identifiants d'accès au backoffice", bold: true }],
              [this.translate.instant('information.prestashop.title'), {
                ul:
                  [
                    { text: this.translate.instant('information.prestashop.sec1.profil'), bold: true },
                    { text: this.translate.instant('information.user') + this.translate.instant('information.prestashop.sec1.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.prestashop.sec1.mdp1') + '\n\n', listType: 'none' },
                    { text: this.translate.instant('information.prestashop.sec1.profil'), bold: true },
                    { text: this.translate.instant('information.user') + this.translate.instant('information.prestashop.sec2.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.prestashop.sec2.mdp1'), listType: 'none' },
                  ]
              }],
              [this.translate.instant('information.woocommerce.title'), {
                ul:
                  [
                    { text: this.translate.instant('information.user') + this.translate.instant('information.woocommerce.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.woocommerce.mdp1'), listType: 'none' },
                  ]
              }],
              [this.translate.instant('information.blog.title'), {
                ul:
                  [
                    { text: this.translate.instant('information.user') + this.translate.instant('information.blog.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.blog.mdp1'), listType: 'none' },
                  ]
              }],
              [this.translate.instant('information.mautic.title'), {
                ul:
                  [
                    { text: this.translate.instant('information.user') + this.translate.instant('information.mautic.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.mautic.mdp1'), listType: 'none' },
                  ]
              }],
              [this.translate.instant('information.suitecrm.title'), {
                ul:
                  [
                    { text: this.translate.instant('information.user') + this.translate.instant('information.suitecrm.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.suitecrm.mdp1'), listType: 'none' },
                  ]
              }],
              [this.translate.instant('information.odoo.title'), {
                ul:
                  [
                    { text: this.translate.instant('information.odoo.sec1.profil'), bold: true },
                    { text: this.translate.instant('information.user') + this.translate.instant('information.odoo.sec1.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.odoo.sec1.mdp1') + '\n\n', listType: 'none' },
                    { text: this.translate.instant('information.odoo.sec2.profil'), bold: true },
                    { text: this.translate.instant('information.user') + this.translate.instant('information.odoo.sec2.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.odoo.sec2.mdp1'), listType: 'none' },
                    { text: this.translate.instant('information.odoo.sec2.info') + this.translate.instant('information.mdp') + this.translate.instant('information.odoo.sec2.mdp2') + '\n\n', listType: 'none' },
                    { text: this.translate.instant('information.odoo.sec3.profil'), bold: true },
                    { text: this.translate.instant('information.user') + this.translate.instant('information.odoo.sec3.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.odoo.sec3.mdp1') + '\n\n', listType: 'none' },
                    { text: this.translate.instant('information.odoo.sec4.profil'), bold: true },
                    { text: this.translate.instant('information.user') + this.translate.instant('information.odoo.sec4.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.odoo.sec4.mdp1') + '\n\n', listType: 'none' },
                    { text: this.translate.instant('information.odoo.sec5.profil'), bold: true },
                    { text: this.translate.instant('information.user') + this.translate.instant('information.odoo.sec5.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.odoo.sec5.mdp1') + '\n\n', listType: 'none' },
                    { text: this.translate.instant('information.odoo.sec6.profil'), bold: true },
                    { text: this.translate.instant('information.user') + this.translate.instant('information.odoo.sec6.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.odoo.sec6.mdp1'), listType: 'none' },
                    { text: this.translate.instant('information.odoo.sec6.info') + this.translate.instant('information.odoo.sec6.mdp2'), listType: 'none' },

                  ]
              }],
              [this.translate.instant('information.kanboard.title'), {
                ul:
                  [
                    { text: this.translate.instant('information.user') + this.translate.instant('information.kanboard.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.kanboard.mdp1'), listType: 'none' },
                  ]
              }],
              [this.translate.instant('information.humhub.title'), {
                ul:
                  [
                    { text: this.translate.instant('information.user') + this.translate.instant('information.humhub.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.humhub.mdp1'), listType: 'none' },
                  ]
              }],
              [{ text: this.translate.instant('information.sftp.title'), bold: true }, {
                ul:
                  [
                    { text: this.translate.instant('information.user') + this.translate.instant('information.sftp.user1'), listType: 'none' },
                    { text: this.translate.instant('information.mdp') + this.translate.instant('information.sftp.mdp1'), listType: 'none' },
                  ]
              }],
            ]
          }
        }
      ]
    }
    // pdfMake.createPdf(docDefinition).open();
    pdfMake.createPdf(docDefinition).download();
  }

}
