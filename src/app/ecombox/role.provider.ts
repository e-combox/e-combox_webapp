import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { map } from "rxjs/operators/map";

import { NbAuthService, NbAuthJWTToken } from "@nebular/auth";
import { NbRoleProvider } from "@nebular/security";

@Injectable()
export class RoleProvider implements NbRoleProvider {
  /**
   *
   * @param {NbAuthService} authService
   */
  constructor(private authService: NbAuthService) {}

  /**
   *
   * @return {Observable<string>}
   */
  getRole(): Observable<string> {
    return this.authService.onTokenChange().pipe(
      map((token: NbAuthJWTToken) => {
        // Get the role from the token
        // 0 : none
        // 1 : admin
        // 2 : user
        return token.isValid() ? token.getPayload()["role"] : 0;
      })
    );
  }
}
