import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { ContainerService } from "../../shared/api/container.service";
@Component({
  selector: "ecx-form-policies",
  styleUrls: ["./form-policies.component.scss"],
  templateUrl: "./form-policies.component.html",
})
/**
 * Form for editing legal notices, T&Cs and privacy policy
 */
export class FormPoliciesComponent implements OnInit {
  public notice = {
    owner: "",
    manager: "",
    purpose: "",
    admin: "",
    dpd: "",
    address: "",
    host: "",
    registration: "",
  };

  private path = "/opt";
  public todo = "à compléter";

  /**
   *
   * @param {StackStore} store
   */
  constructor(
    private containerService: ContainerService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.toastr.toastrConfig.timeOut = 10000;
  }

  /**
   *
   */
  ngOnInit(): void {
    for (const propt in this.notice) {
      if (Object.prototype.hasOwnProperty.call(this.notice, propt)) {
        this.getData(propt);
      }
    }
  }

  /**
   *
   */
  confirmEdit(): void {
    for (const propt in this.notice) {
      if (Object.prototype.hasOwnProperty.call(this.notice, propt)) {
        this.setData(propt);
      }
    }
  }

  /**
   *
   */
  close(): void {
    this.router.navigate(["ecombox/policies"]);
  }

  /**
   *
   * @param {string} file
   */
  private setData(file: string): void {
    if (this.notice[file] !== "") {
      this.containerService
        .runExecInstance(
          "FSserver",
          `echo "${this.notice[file]}" > ${this.path}/${file}`,
          false,
          "/bin/sh"
        )
        .subscribe(() => {
          this.toastr.info(`Les informations ont été correctement modifiées.`);
        });
    }
  }

  /**
   *
   * @param {string} file
   */
  private getData(file: string): void {
    this.containerService
      .runExecInstance("FSserver", `cat ${this.path}/${file}`, false, "/bin/sh")
      .subscribe((result) => {
        if (!result.includes("No such file or directory")) {
          this.notice[file] = result.slice(8);
        }
      });
  }
}
