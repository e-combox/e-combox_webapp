import { NgModule } from "@angular/core";

import { StacksService } from "./api/stacks.service";
import { StackStore } from "./store/stack.store";
import { ContainerService } from "./api/container.service";
import { SystemService } from "./api/system.service";
import { VolumeService } from "./api/volume.service";
import { DockerRegistryService } from "./api/docker-registry.service";
import { ImageService } from "./api/image.service";

/**
 * Contains all services and elements shared between all application modules
 */
@NgModule({
  providers: [
    StacksService,
    StackStore,
    ContainerService,
    SystemService,
    VolumeService,
    DockerRegistryService,
    ImageService,
  ],
})
export class SharedModule {}
