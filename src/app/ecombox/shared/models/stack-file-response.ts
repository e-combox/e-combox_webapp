export interface StackFileResponse {
  /**
   * Content of the Stack file
   */
  StackFileContent?: string;
}
