import { StackEnv } from "./stack-env";

export interface StackCreateRequest {
  /**
   * Name of the stack
   */
  name: string;
  /**
   * Swarm cluster identifier. Required when creating a Swarm stack (type 1).
   */
  swarmID?: string;
  /**
   * Content of the Stack file. Required when using the 'string' deployment method.
   */
  stackFileContent?: string;
  /**
   * URL of a Git repository hosting the Stack file. Required when using the 'repository' deployment method.
   */
  repositoryURL?: string;
  /**
   * Reference name of a Git repository hosting the Stack file. Used in 'repository' deployment method.
   */
  repositoryReferenceName?: string;
  /**
   * Path to the Stack file inside the Git repository. Will default to 'docker-compose.yml' if not specified.
   */
  composeFile?: string;
  /**
   * Use basic authentication to clone the Git repository.
   */
  repositoryAuthentication?: boolean;
  /**
   * Username used in basic authentication. Required when RepositoryAuthentication is true.
   */
  repositoryUsername?: string;
  /**
   * Password used in basic authentication. Required when RepositoryAuthentication is true.
   */
  repositoryPassword?: string;
  /**
   * A list of environment variables used during stack deployment
   */
  env?: Array<StackEnv>;
}
