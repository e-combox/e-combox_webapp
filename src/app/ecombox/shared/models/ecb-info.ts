import { Injectable } from "@angular/core";
import { Adapter } from "./adapter";

export interface EcbInfo {
  displayMessageUpdate?: boolean;
  messageUpdate?: string;
  urlChangelog?: string;
  urlHelpUpdateWeb?: string;
  urlHelpUpdateEcb?: string;
  versionEcb?: string;
  versionScript?: string;
  levelUpdate?: number;
  displayNotification?: boolean;
  repositoryUsername?: string;
  messageNotification?: string;
  serverTypeNotification: string;
}

@Injectable({
  providedIn: "root",
})
export class EcbInfoAdapter implements Adapter<EcbInfo> {
  /**
   * Return EcbInfo from json object
   * @param {any} item
   * @return {EcbInfo}
   */
  adapt(item): EcbInfo {
    const result: EcbInfo = {
      displayMessageUpdate: item.display,
      messageUpdate: item.message,
      urlChangelog: item.changelog,
      urlHelpUpdateWeb: item.helpUpdateWeb,
      urlHelpUpdateEcb: item.helpUpdateEcb,
      versionEcb: item.version,
      versionScript: item.scriptVersion,
      levelUpdate: item.level,
      displayNotification: item.serverNotification.display,
      messageNotification: item.serverNotification.message,
      serverTypeNotification: item.serverNotification.serverType,
    };

    return result;
  }
}
