export interface StackEnv {
  name?: string;
  value?: string;
}
