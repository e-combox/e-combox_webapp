/* eslint-disable camelcase */
/* eslint-disable @typescript-eslint/no-explicit-any */
export class Utils {
  /**
   * Get a random string : used for generate passwords
   * @param {number} length wanted length for the remandom string
   * @return {string}
   */
  public static getRandomString(length: number): string {
    const chars =
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    let result = "";
    for (let i = 0; i < length; i++) {
      result += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return result;
  }

  /**
   *
   * @param {number} bytes
   * @param {number} decimals
   * @return {string}
   */
  public static formatBytes(bytes: number, decimals = 2): string {
    if (bytes === 0) return "Pas d'espace occupé";

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Octets", "Ko", "Mo", "Go", "To", "Po", "Eo", "Zo", "Yo"];

    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  /**
   *
   * @param {any} stats
   * @param {any} prec_stats
   * @return {number}
   */
  public static calculCPUUsage(stats: any, prec_stats: any): number {
    let cpuPercent = 0.0;
    const cpuDelta =
      stats.cpu_usage.total_usage - prec_stats.cpu_usage.total_usage;
    const systemDelta = stats.system_cpu_usage - prec_stats.system_cpu_usage;

    if (systemDelta > 0.0 && cpuDelta > 0.0) {
      cpuPercent = (cpuDelta / systemDelta) * stats.online_cpus * 100.0;
    }

    return cpuPercent;
  }
}
