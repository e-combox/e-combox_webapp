import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({ providedIn: "root" })
export class LoaderService {
  // Observable with default value
  public isLoading = new BehaviorSubject(false);
}
