/* eslint-disable @typescript-eslint/no-explicit-any */
import { Inject, Injectable, Optional } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { ApiService } from "./api.service";
import { Image } from "../models/image";
import { CustomHttpUrlEncodingCodec } from "../../../encoder";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map, mergeMap, concatMap, catchError } from "rxjs/operators";
import { Configuration } from "./configuration";
import { BASE_PATH, SERVER_TYPES, URL_SITE } from "./variables";
import { ContainerService } from "./container.service";
import { NbTokenStorage } from "@nebular/auth";
import { ToastrService } from "ngx-toastr";
import { NbAuthService } from "@nebular/auth";
import { TranslateService } from "@ngx-translate/core";
import { DockerRegistryService } from "./docker-registry.service";
import { DialogSameNameComponent } from "../../images/dialog-same-name/dialog-same-name.component";
import { NbDialogService } from "@nebular/theme";
import { StackStore } from "../store/stack.store";

@Injectable()
export class ImageService extends ApiService {
  private endpoint: string;
  subject = new Subject<Array<string>>();
  /**
   * Constructor
   * @param {HttpClient} httpClient
   * @param {ContainerService} containerService
   * @param {string} basePath Optional
   * @param {Configuration} configuration Optional
   * @param {ContainerAdapter} adapter
   * @param {TranslateService} translate
   */
  constructor(
    protected httpClient: HttpClient,
    private containerService: ContainerService,
    protected service: NbAuthService,
    protected tokenService: NbTokenStorage,
    private toastr: ToastrService,
    public translate: TranslateService,
    private registryService: DockerRegistryService,
    private dialogService: NbDialogService,
    private store: StackStore,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration
  ) {
    super(httpClient, basePath, configuration);
    this.endpoint = `${this.basePath}/endpoints/1/docker`;
  }

  /**
   * @param {string} nameContainer
   * @param {string} serverType
   * @param {string} tagImage
   * @param {string} portRepo
   * @return {Observable<Image>}
   */
  public imageCreate(
    nameContainer: string,
    serverType: string,
    tagImage: string,
    portRepo: string
  ): Observable<any> {
    const cmd = "/tmp/prepare-image.sh";
    const nameImage = `localhost:${portRepo}%2F${serverType}`;
    let execOpt = false;
    if (serverType === SERVER_TYPES.odoo) {
      execOpt = true;
    }

    return this.containerService
      .runExecInstance(nameContainer, cmd, execOpt)
      .pipe(
        mergeMap(() =>
          this.imageCommit(nameContainer, nameImage, tagImage).pipe(
            mergeMap(() => this.imagePush(nameImage + ":" + tagImage))
          )
        )
      );
  }

  /**
   * @param {string} nameContainer
   * @param {string} serverType
   * @param {string} tagImage
   * @param {string} portRepo
   * @return {Observable<Image>}
   */
  public imageCreateDB(
    nameContainer: string,
    serverType: string,
    tagImage: string,
    portRepo: string
  ): Observable<any> {
    const sqlFile = "/docker-entrypoint-initdb.d/" + serverType + "-db.sql";
    let cmd;

    if (serverType === SERVER_TYPES.odoo) {
      /* DO NOT TOUCH !! */
      cmd = "pg_dumpall -f " + sqlFile;
      cmd += "&& sed -i '14,15s/.*//' " + sqlFile;
      cmd +=
        "&& sed -i '5s/.*/\\\\set passwd `echo \"\\$DB_PASS\"`/' " + sqlFile;
      cmd +=
        '&& sed -i "15s/.*/ALTER USER \\"userOdoo\\" WITH PASSWORD :\'passwd\';/" ' +
        sqlFile;
    } else {
      cmd = `mysqldump -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE > ${sqlFile}`;
    }

    const nameImage = `localhost:${portRepo}%2F${serverType}-db`;

    return this.containerService
      .runExecInstance(nameContainer, cmd)
      .pipe(
        mergeMap(() =>
          this.imageCommit(nameContainer, nameImage, tagImage).pipe(
            mergeMap(() => this.imagePush(nameImage + ":" + tagImage))
          )
        )
      );
  }
  /**
   * @param {string} id Id of the container used for image creation
   * @param {string} name Name of the new image tag
   * @param {string} tag
   * @return {Observable<any>}
   */
  public imageCommit(id: string, name: string, tag: string): Observable<Image> {
    /* let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });*/

    // queryParameters = queryParameters.set("container", id);
    // queryParameters = queryParameters.set("repo", name);
    name = name.replace("%2F", "/");
    const headers = this.configureDefaultHeaders();

    return this.httpClient.post<Image>(
      `${this.endpoint}/commit?container=${id}&repo=${name}&tag=${tag}`,
      {
        // params: queryParameters,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
      }
    );
  }

  /**
   * @param {string} name Name of the image to push
   * @param {string} share Optional
   * @param {string} nameDocker Optional
   * @param {string} password Optional
   * @return {Observable<any>}
   */
  public imagePush(
    name: string,
    share?: boolean,
    nameDocker?: string,
    password?: string
  ): Observable<any> {
    let headers = this.configureDefaultHeaders();
    name = name.replace("%2F", "/");

    if (nameDocker && password) {
      nameDocker = nameDocker.replace(/\"/g, '\\"');
      password = password.replace(/\"/g, '\\"');
      const registry = `{"username":"${nameDocker}","password":"${password}","serveraddress":"${URL_SITE.platform_url}"}`;
      const auth = btoa(registry);
      headers = headers.set("X-Registry-Auth", auth);
    }

    const body = {};
    let params = {};
    if (share) {
      params = {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        reportProgress: false,
        responseType: "text",
      };
    } else {
      params = {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        reportProgress: false,
      };
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return this.httpClient.post(
      `${this.endpoint}/images/${name}/push`,
      body,
      params
    );
  }

  /**
   *
   * @param {string} oldName
   * @param {string} newName
   * @param {string} body
   * @return {Observable<any>}
   */
  private imageTag(oldName: string, newName: string): Observable<any> {
    // oldName = encodeURIComponent(oldName);
    // newName = encodeURIComponent(newName);
    const body = {};
    return this.httpClient.post<any>(
      `${this.endpoint}/images/${oldName}/tag?repo=${newName}`,
      body,
      {
        withCredentials: true,
      }
    );
  }

  /**
   * @param {string} filters
   * @return {Observable<any>}
   */
  public imagesList(filters?: string): Observable<Image> {
    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });

    if (filters !== undefined && filters !== null) {
      queryParameters = queryParameters.set("filters", <string>filters);
    }

    const headers = this.configureDefaultHeaders();

    return this.httpClient.get<Image>(`${this.basePath}${this.endpoint}`, {
      params: queryParameters,
      withCredentials: this.configuration.withCredentials,
      headers: headers,
      observe: "body",
      reportProgress: false,
    });
  }

  /**
   *
   * @param {string} name
   * @return {Observable<any>}
   */
  private imagePull(name: string): Observable<any> {
    return this.httpClient.post(
      `${this.endpoint}/images/create?fromImage=${name}`,
      {},
      {
        responseType: "text",
      }
    );
  }

  /**
   *
   * @param {string} username
   * @param {string} siteType
   * @return {Observable<any>}
   */
  public getlisttags(username: string, siteType: string): Observable<any> {
    const constructUrl = this.basePath.split("/");
    let urlEcomBox = constructUrl[2];
    const path = constructUrl[3];
    if (path !== "portainer") {
      urlEcomBox += `/${path}`;
    }
    return this.httpClient.get<any>(
      `https://${urlEcomBox}/dockerhub/v2/repositories/${username}/${siteType}/tags`
    );
  }

  /**
   *
   * @param {string} localPort
   * @param {string} siteType
   * @param {string} modelName
   * @return {string}
   */
  public imageLocalName(
    localPort: string,
    siteType: string,
    modelName: string
  ): string {
    return `${URL_SITE.domain}:${localPort}/${siteType}:${modelName}`;
  }

  /**
   *
   * @param {string} username
   * @param {string} siteType
   * @param {string} modelName
   * @return {string}
   */
  public imageExportName(
    username: string,
    siteType: string,
    modelName: string
  ): string {
    return `${URL_SITE.platform_url}/${username}/${siteType}:${modelName}`;
  }

  /**
   *
   * @param {string} siteType
   * @param {string} modelName
   * @param {string} domain
   * @param {string} port
   * @param {boolean} add
   * @return {Oservable<any>}
   */
  public importSuccess(
    siteType: string,
    modelName: string,
    domain: string,
    port: string,
    add: boolean
  ): Observable<any> {
    return this.registryService.registryTags(siteType, domain, port).pipe(
      map((value) => {
        const exist = value.tags.includes(modelName);
        if (exist) {
          this.toastr.success(
            this.translate.instant("message.success") + "importé !"
          );
          if (add) {
            this.subject.next([modelName, siteType]);
          }
        } else {
          this.toastr.error(this.translate.instant("message.echecI"));
        }
      })
    );
  }

  /**
   *
   * @param {string} oldName
   * @param {string} newName
   * @param {string} username Optional
   * @param {string} password Optional
   * @param {string} siteType Optional
   * @param {string} domain Optional
   * @param {string} port Optional
   * @param {boolean} change Optional
   * @return {Observable<any>}
   */
  public imageShareUpDate(
    oldName: string,
    newName: string,
    username?: string,
    password?: string,
    siteType?: string,
    domain?: string,
    port?: string,
    change?: boolean
  ): Observable<any> {
    return this.imagePull(oldName).pipe(
      mergeMap((res) => {
        const debut = res.search("sha256:");
        const fin = res.lastIndexOf("status");
        const digest = res.slice(debut, fin - 6);
        return this.registryService
          .existImage(domain, port, siteType, digest)
          .pipe(
            concatMap(() =>
              this.registryService.deleteDigest(domain, port, siteType, digest)
            ),
            map(() => {
              if (!change) {
                if (siteType.includes("-db")) {
                  this.toastr.info(
                    "La base de donnée du site existe déjà dans votre dépôt privé. Elle va être supprimée et remplacée."
                  );
                } else {
                  this.toastr.info(
                    "Le modèle de site existe déjà dans votre dépôt privé. Il va être supprimé et remplacé."
                  );
                }
              }
            }),
            concatMap(() => this.imageTag(oldName, newName)),
            concatMap(() => this.imagePush(newName, true, username, password))
          );
      }),
      catchError(() => {
        return this.imageTag(oldName, newName).pipe(
          mergeMap(() => this.imagePush(newName, true, username, password))
        );
      })
    );
  }

  /**
   *
   * @param {Array<string>} res
   * @return {Observable<any>}
   */
  public onSameName(res: Array<string>): Observable<any> {
    return this.dialogService.open(DialogSameNameComponent, {
      context: {
        images: res,
      },
      closeOnBackdropClick: false,
    }).onClose;
  }

  /**
   *
   * @return { Array<string> }
   */
  public urlBasePath(): Array<string> {
    let domain = "";
    let port: string = null;
    if (this.store.getPath() !== "") {
      domain = this.store.getFQDN();
    } else {
      if (
        this.store.getPublicURL() !== "" &&
        this.store.getPublicURL() !== undefined
      ) {
        domain = this.store.getPublicURL();
      } else {
        domain = this.store.getDockerIP();
      }

      port = this.store.getNginxPort();
    }
    return [domain, port];
  }
}
