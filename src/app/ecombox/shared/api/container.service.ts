/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable, Optional, Inject } from "@angular/core";
import { ApiService } from "./api.service";
import { HttpClient, HttpParams } from "@angular/common/http";
import { BASE_PATH } from "./variables";
import { Configuration } from "./configuration";
import { Observable, forkJoin } from "rxjs";
import { CustomHttpUrlEncodingCodec } from "../../../encoder";
import { Container } from "../models/container";
import { ContainerAdapter } from "../models/container";
import { map, mergeMap } from "rxjs/operators";
import { ExecRequest } from "../models/exec-request";

@Injectable()
export class ContainerService extends ApiService {
  private endpoint: string;

  /**
   * Constructor
   * @param {HttpClient} httpClient
   * @param {string} basePath Optional
   * @param {Configuration} configuration Optional
   * @param {ContainerAdapter} adapter
   */
  constructor(
    protected httpClient: HttpClient,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration,
    private adapter: ContainerAdapter
  ) {
    super(httpClient, basePath, configuration);
    this.endpoint = `${this.basePath}/endpoints/1/docker/containers`;
  }

  /**
   * Get containers from API
   * @param {string} filters
   * @return {Observable<Container[]>}
   */
  public containerGet(filters?: string): Observable<Container[]> {
    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });
    if (filters !== undefined && filters !== null) {
      queryParameters = queryParameters.set("filters", <string>filters);
    }
    queryParameters = queryParameters.set("all", "true");
    // queryParameters = queryParameters.set("size", "true");

    const headers = this.configureDefaultHeaders();

    return this.httpClient
      .get<Container[]>(`${this.endpoint}/json`, {
        params: queryParameters,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
      })
      .pipe(
        // Adapt each item in the raw data array
        map((data) => data.map((item) => this.adapter.adapt(item)))
        // mergeMap(item => )
      );
  }

  /**
   * Get statistics of containers usage
   * @param {Array<Container>} containers
   * @return {Observable<any[]>}
   */
  public containerStats(containers: Array<string>): Observable<any> {
    const urls: Array<Observable<Container>> = [];
    const headers = this.configureDefaultHeaders();
    containers.forEach((id: string) => {
      urls.push(
        this.httpClient.get<any>(`${this.endpoint}/${id}/stats?stream=false`, {
          withCredentials: this.configuration.withCredentials,
          headers: headers,
        })
      );
    });

    return forkJoin(urls);
  }

  /**
   *
   * @param {string} name
   * @return {Observable<aContainer>}
   */
  public containerInspect(name: string): Observable<Container> {
    const headers = this.configureDefaultHeaders();

    return this.httpClient
      .get<Container>(`${this.endpoint}/${name}/json`, {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
      })
      .pipe(map((value) => this.adapter.adapt(value)));
  }

  /**
   * @param {string} id
   * @return {Observable<any>}
   */
  public containerStart(id: string): Observable<Container> {
    const headers = this.configureDefaultHeaders();

    return this.httpClient.post<Container>(`${this.endpoint}/${id}/start`, {
      withCredentials: this.configuration.withCredentials,
      headers: headers,
    });
  }

  /**
   * @param {string} id
   * @return {Observable<Container>}
   */
  public containerStop(id: string): Observable<Container> {
    const headers = this.configureDefaultHeaders();

    return this.httpClient.post<Container>(`${this.endpoint}/${id}/stop`, {
      withCredentials: this.configuration.withCredentials,
      headers: headers,
    });
  }

  /**
   * Create and run an exec Docker instance
   * @param {string} name Name of the container to exec the command
   * @param {string} cmd The command to exec
   * @param {boolean} withoutOpt
   * @param {string} typeCmd
   * @return {Observable<any>}
   */
  public runExecInstance(
    name: string,
    cmd: string,
    withoutOpt = false,
    typeCmd = "/bin/bash"
  ): Observable<string> {
    let cmdOption;
    if (withoutOpt) {
      cmdOption = [typeCmd, cmd];
    } else {
      cmdOption = [typeCmd, "-c", cmd];
    }
    const paramsExec = {
      AttachStdin: false,
      AttachStdout: true,
      AttachStderr: true,
      Tty: false,
      Cmd: cmdOption,
    };

    const body = JSON.stringify(paramsExec);

    const headers = this.configureDefaultHeaders();
    return this.httpClient
      .post<ExecRequest>(`${this.endpoint}/${name}/exec`, body, {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
      })
      .pipe(mergeMap((data: ExecRequest) => this.startExec(data.Id)));
  }

  /**
   * Run an exec instance
   * @param {string} id ID of the exec instance
   * @return {Observable<any>}
   */
  private startExec(id: string): Observable<string> {
    const startExec = {
      Detach: false,
      Tty: false,
    };
    const body = JSON.stringify(startExec);
    const headers = this.configureDefaultHeaders();

    return this.httpClient.post<string>(
      `${this.basePath}/endpoints/1/docker/exec/${id}/start`,
      body,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        responseType: "text" as "json",
      }
    );
  }
}
