/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpResponse,
  HttpErrorResponse,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { NbAuthJWTToken, NbAuthService } from "@nebular/auth";
import { Router } from "@angular/router";
import { LoaderService } from "../services/loader.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  private token: string;
  private requests: string[] = [];
  /**
   * Constructor
   * @param {AuthService} auth
   */
  constructor(
    private toastr: ToastrService,
    private authService: NbAuthService,
    private loaderService: LoaderService,
    private router: Router
  ) {
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        this.token = token.toString(); // here we receive a payload from the token and assigns it to our `user` variable
      }
    });
  }

  /**
   * Add token authorization to header request
   * @param {HttpRequest} request
   * @param {string} token
   * @return {HttpRequest}
   */
  private addToken(
    request: HttpRequest<any>,
    token: string
  ): HttpRequest<unknown> {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`,
      },
    });
  }

  /**
   * Revome request from array when is done
   * @param {HttpRequest} req
   */
  private removeRequest(req: HttpRequest<unknown>): void {
    const i = this.requests.indexOf(req.url);

    if (i >= 0) {
      this.requests.splice(i, 1);

      // change status of loading
      this.loaderService.isLoading.next(this.requests.length > 0);
    }
  }

  /**
   * Intercept every request to add token authorization
   * @param {HttpRequest} request
   * @param {HttpHandler} next
   * @return {Observable}
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    this.requests.push(request.url);
    this.loaderService.isLoading.next(true);

    if (
      this.token &&
      !request.url.includes("registry.hub.docker.com/v2/repositories")
    ) {
      request = this.addToken(request, this.token);
    }

    return next.handle(request).pipe(
      map((event) => {
        if (event instanceof HttpResponse) {
          this.removeRequest(request);
          if (event.headers) {
            // Get digest for delete image from registry
            event.headers.get("Docker-Content-Digest");
          }
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        // this.auth.requestsPending -= 1;
        this.removeRequest(request);
        if (error instanceof HttpErrorResponse) {
          let message = "";
          switch (error.status) {
            case 401:
              this.router.navigate(["auth/login"]);
              break;
            case 409:
              message =
                "Un site avec le même nom existe déjà. Veuillez choisir un autre nom.";
              break;
            case 500:
              if (error.error.details.includes("is already in progress")) {
                console.error(error.error.details);
              } else {
                message =
                  "Une erreur est survenue. Veuillez retenter l'opération.";
              }
              break;
            default:
              return throwError(error);
          }
          if (message.length > 0) {
            this.toastr.error(message);
          }
        }
        return throwError(error);
      })
    );
  }
}
