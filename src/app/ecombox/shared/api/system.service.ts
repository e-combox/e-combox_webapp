/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
import { HttpParams } from "@angular/common/http";
import { CustomHttpUrlEncodingCodec } from "../../../encoder";
import { catchError } from "rxjs/operators";

@Injectable()
export class SystemService extends ApiService {
  private endpoint = `/endpoints/1/docker`;

  /**
   * Get Data Usage of Docker servcie - WARNING : response time is very long
   * @return {Observable<DataUsage>}
   */
  public systemGetDataUsage(): Observable<any> {
    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });
    queryParameters = queryParameters.set("stream", "false");

    const headers = this.configureDefaultHeaders();
    return this.httpClient.get<any>(
      `${this.basePath}${this.endpoint}/system/df`,
      {
        params: queryParameters,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
      }
    );
  }

  /**
   * Get Data Usage of Docker servcie - WARNING : response time is very long
   * @return {Observable<any>}
   */
  public systemGetInfo(): Observable<any> {
    const headers = this.configureDefaultHeaders();
    return this.httpClient.get<any>(`${this.basePath}${this.endpoint}/info`, {
      withCredentials: this.configuration.withCredentials,
      headers: headers,
    });
  }

  /**
   * Search e-comBox update
   * @return {string}
   */
  searchUpdate(): Observable<any> {
    return this.httpClient
      .get(
        "https://cors-anywhere.btssio.corsica/forge.aeif.fr/e-combox/e-combox_update/-/raw/master/informations.json"
      )
      .pipe(catchError(this.handleError<any>("getInformationsMessage")));
  }
}
