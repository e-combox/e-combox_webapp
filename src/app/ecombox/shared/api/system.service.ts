/* eslint-disable @typescript-eslint/no-explicit-any */
import { Inject, Injectable, Optional } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
import { HttpClient, HttpParams } from "@angular/common/http";
import { CustomHttpUrlEncodingCodec } from "../../../encoder";
import { catchError, map, tap } from "rxjs/operators";
import { EcbInfo, EcbInfoAdapter } from "../models/ecb-info";
import { Configuration } from "./configuration";
import { BASE_PATH } from "./variables";

@Injectable()
export class SystemService extends ApiService {
  private endpoint = `/endpoints/1/docker`;

  /**
   *
   * @param {HttpClient} httpClient
   * @param {string} basePath
   * @param {Configuration} configuration
   * @param {EcbInfoAdapter} adapter
   */
  constructor(
    protected httpClient: HttpClient,
    @Optional() @Inject(BASE_PATH) basePath: string,
    @Optional() configuration: Configuration,
    private adapter: EcbInfoAdapter
  ) {
    super(httpClient, basePath, configuration);
  }

  /**
   * Get Data Usage of Docker servcie - WARNING : response time is very long
   * @return {Observable<DataUsage>}
   */
  public systemGetDataUsage(): Observable<any> {
    let queryParameters = new HttpParams({
      encoder: new CustomHttpUrlEncodingCodec(),
    });
    queryParameters = queryParameters.set("stream", "false");

    const headers = this.configureDefaultHeaders();
    return this.httpClient.get<any>(
      `${this.basePath}${this.endpoint}/system/df`,
      {
        params: queryParameters,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
      }
    );
  }

  /**
   * Get Data Usage of Docker servcie - WARNING : response time is very long
   * @return {Observable<any>}
   */
  public systemGetInfo(): Observable<any> {
    const headers = this.configureDefaultHeaders();
    return this.httpClient.get<any>(`${this.basePath}${this.endpoint}/info`, {
      withCredentials: this.configuration.withCredentials,
      headers: headers,
    });
  }

  /**
   * Search e-comBox update
   * @param {string} domain
   * @param {string} port
   * @return {string}
   */
  searchUpdate(): Observable<EcbInfo> {
    const constructUrl = this.basePath.split("/");
    let urlEcomBox = constructUrl[2];
    const path = constructUrl[3];
    if (path !== "portainer") {
      urlEcomBox += `/${path}`;
    }
    /* return this.httpClient
      .get(
        `https://cors-anywhere.btssio.corsica/forge.aeif.fr/e-combox/e-combox_update/-/raw/master/informations.json`
      )
      .pipe(
        // Adapt item in the raw data array
        map((item) => this.adapter.adapt(item)),
        catchError(this.handleError<any>("getInformationsMessage"))
      );*/
    return this.httpClient
      .get(
        `https://${urlEcomBox}/forge/e-combox/e-combox_update/-/raw/master/informations.json`
      )
      .pipe(
        // Adapt item in the raw data array
        tap((data) => this.adapter.adapt(data)),
        catchError(this.handleError<any>("getInformationsMessage"))
      );
  }
}
