import { NbMenuService } from "@nebular/theme";
import { Component } from "@angular/core";

@Component({
  selector: "ngx-not-found",
  styleUrls: ["./not-found.component.scss"],
  templateUrl: "./not-found.component.html",
})
export class NotFoundComponent {
  /**
   *
   * @param {NbMenuService} menuService
   */
  constructor(private menuService: NbMenuService) {}

  /**
   * Used to back to home section
   * No Param - void return
   */
  goToHome(): void {
    this.menuService.navigateHome();
  }
}
