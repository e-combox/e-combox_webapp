import { Component, OnDestroy, OnInit } from "@angular/core";
import { StackStore } from "../../shared/store/stack.store";
import { Subscription } from "rxjs";
import { Stack, TypeStack } from "../../shared/models/stack";
import { environment } from "../../../../environments/environment";
import { SystemService } from "../../shared/api/system.service";
import { NbComponentStatus } from "@nebular/theme";
import { NbAuthJWTToken, NbAuthService } from "@nebular/auth";
import { EcbInfo } from "../../shared/models/ecb-info";
import semver from "semver";

@Component({
  selector: "ecx-ecombox-state",
  templateUrl: "./ecombox-state.component.html",
  styleUrls: ["./ecombox-state.component.scss", "animate.css"],
})
/**
 * Displays the main information about the configuration of the e-comBox solution
 */
export class EcomboxStateComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  public stacks: Stack[];
  public nbContainers = 0;
  public urlEcomBox: string;
  public urlHelp: string;
  public urlChangelog: string;
  public updateClass: string;
  public isBeta = false;
  public isImportant = false;
  public webappUpdate = false;
  public ecbUpdate = false;
  public boxUpdate = "progress-info";
  public levelUpdate: NbComponentStatus;
  public classVersion = "title-config-etat";
  public labelVersion = "Version : ";
  public currentUser = {
    username: "",
  };

  version: string = environment.ecomboxVersion;
  scriptVersion = "";
  messVersion: string;

  /**
   * @param {StackStore} stackStore
   * @param {SystemService} systemService
   */
  constructor(
    private stackStore: StackStore,
    private systemService: SystemService,
    private authService: NbAuthService
  ) {
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        // here we receive a payload from the token and assigns it to our `user` variable
        this.currentUser = token.getPayload();
      }
    });
  }

  /**
   * @return {void}
   */
  ngOnInit(): void {
    this.subscription = this.stackStore.getStacks().subscribe((stacks) => {
      this.nbContainers = 0;
      this.stacks = stacks;

      // we get api's URL to display it on the dashboard but only the domain name (or IP address)
      if (this.stackStore.getPath() !== "") {
        this.urlEcomBox = this.stackStore.getFQDN();
      } else {
        const constructUrl = this.systemService.basePath.split("/");
        const host = constructUrl[2].split(":");
        this.urlEcomBox = host[0];
      }

      if (
        this.stackStore.getScriptVersion() &&
        this.scriptVersion.length === 0
      ) {
        this.scriptVersion = this.stackStore.getScriptVersion();

        this.systemService.searchUpdate().subscribe(
          (response) => {
            this.displayUpdate(response);
            this.displayBanner(response);
          },
          (error: Error) => {
            console.error(error.message);
          }
        );
      }

      stacks.forEach((stack) => {
        if (stack.getTypeStack() === TypeStack.default) {
          this.nbContainers += 1;
        }
      });
    });
  }

  /**
   * Check if a new version is available
   * @param {EcbInfo} info
   */
  private displayUpdate(info: EcbInfo): void {
    console.dir(info);
    if (info) {
      this.messVersion = info.messageUpdate;
      this.urlChangelog = info.urlChangelog;
      let cleanScriptVersion = this.scriptVersion;
      if (this.scriptVersion.startsWith("dev")) {
        cleanScriptVersion = this.scriptVersion.replace("dev.", "");
      }
      // Dev version
      if (semver.gt(this.version, info.versionEcb)) {
        this.isBeta = true;
        this.version += "-beta";
        // Install scripts update
      } else if (semver.lt(cleanScriptVersion, info.versionScript)) {
        this.urlHelp = info.urlHelpUpdateEcb;
        this.ecbUpdate = true;

        this.displayImportantUpdate(
          cleanScriptVersion,
          info.versionScript,
          info.levelUpdate
        );

        // Webapp update
      } else if (semver.lt(this.version, info.versionEcb)) {
        this.urlHelp = info.urlHelpUpdateWeb;
        this.webappUpdate = true;

        this.displayImportantUpdate(
          this.version,
          info.versionEcb,
          info.levelUpdate
        );
      }
    }
  }

  /**
   * @param {string} installedVersion
   * @param {string} actualVersion
   * @param {number} level
   */
  private displayImportantUpdate(
    installedVersion: string,
    actualVersion: string,
    level: number
  ): void {
    const typeVersion = semver.diff(installedVersion, actualVersion);

    if (level === 3 || typeVersion === "major") {
      this.isImportant = true;
      this.boxUpdate += " major";
      this.labelVersion = "Une mise à jour importante est disponible ! ";
      this.levelUpdate = "control";
      this.updateClass = "update-major";
      this.classVersion = "version";
    }
  }

  /**
   *
   * @param {EcbInfo} info
   */
  private displayBanner(info: EcbInfo): void {
    if (info.displayNotification) {
      localStorage.setItem("ecombox.notification.display", "true");
      localStorage.setItem(
        "ecombox.notification.server",
        info.serverTypeNotification
      );
    } else {
      localStorage.setItem("ecombox.notification.display", "false");
    }
  }

  /**
   * @return {void}
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * No Param - void return
   */
  onRefresh(): void {
    // this.getInfo();
  }

  /**
   *
   */
  public openHelpInstall(): void {
    console.log(this.urlHelp);
    window.open(this.urlHelp, "_blank");
  }
}
