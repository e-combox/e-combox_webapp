import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { ProgressInfo } from "../../../@core/data/stats-progress-bar";
import { SERVER_TYPES, TAB_SERVERS } from "../../shared/api/variables";
import { StackStore } from "../../shared/store/stack.store";
import { I18nPluralPipe } from "@angular/common";

@Component({
  selector: "ecx-ecombox-server-details",
  templateUrl: "./ecombox-server-details.component.html",
  styleUrls: ["./ecombox-server-details.component.scss"],
  providers: [I18nPluralPipe],
})
/**
 * Displays statistics about servers: number of sites created, started and stopped
 */
export class EcomboxServerDetailsComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  progressInfoData: ProgressInfo[];
  public serversStarted: Record<TAB_SERVERS, number>;
  public serversStopped: Record<TAB_SERVERS, number>;
  messageMapping: { [k: string]: string } = {
    "=0": "Aucun site installé",
    "=1": "# installé",
    other: "# installés",
  };

  descriptionMapping: { [k: string]: string } = {
    "=1": "# site démarré",
    other: "# sites démarrés",
  };

  /**
   *
   */
  constructor(private store: StackStore, private pluralPipe: I18nPluralPipe) {}

  /**
   *
   */
  ngOnInit(): void {
    this.subscription = this.store.getStacks().subscribe((stacks) => {
      this.initRecords();
      if (stacks !== undefined) {
        stacks.forEach((stack) => {
          if (stack !== undefined) {
            if (stack.getStatus() === 1) {
              this.serversStarted[stack.getServerType()] += 1;
            } else {
              this.serversStopped[stack.getServerType()] += 1;
            }
          }
        });
      }
      this.buildProgressBars();
    });
  }

  /**
   * Init records
   */
  private initRecords(): void {
    this.serversStarted = {
      woocommerce: 0,
      blog: 0,
      humhub: 0,
      kanboard: 0,
      mautic: 0,
      odoo: 0,
      pma: 0,
      prestashop: 0,
      sftp: 0,
      suitecrm: 0,
    };
    this.serversStopped = {
      woocommerce: 0,
      blog: 0,
      humhub: 0,
      kanboard: 0,
      mautic: 0,
      odoo: 0,
      pma: 0,
      prestashop: 0,
      sftp: 0,
      suitecrm: 0,
    };
  }

  /**
   *
   */
  private buildProgressBars(): void {
    this.progressInfoData = [];
    for (const key in this.serversStarted) {
      if (key && key !== SERVER_TYPES.pma && key !== SERVER_TYPES.sftp) {
        const nbRunning = this.serversStarted[key];
        this.progressInfoData.push({
          title: key.charAt(0).toUpperCase() + key.substring(1),
          value: nbRunning + this.serversStopped[key],
          activeProgress:
            (nbRunning * 100) / (nbRunning + this.serversStopped[key]),
          description: `${this.pluralPipe.transform(
            nbRunning,
            this.descriptionMapping
          )} sur ${nbRunning + this.serversStopped[key]}`,
        });
      }
    }
  }

  /**
   *
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
