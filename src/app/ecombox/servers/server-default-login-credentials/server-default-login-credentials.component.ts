import { Component, Input } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "ecx-server-default-login-credentials",
  templateUrl: "./server-default-login-credentials.component.html",
  styleUrls: ["./server-default-login-credentials.component.scss"],
})
export class ServerDefaultLoginCredentialsComponent {
  @Input() type: string;

  /**
   *
   * @param {translate} undefined
   */
  constructor(public translate: TranslateService) {}

  /**
   *
   */
  ngOnInit(): void {
    if (this.type === "prestashop") {
      this.items = [
        {
          title: this.translate.instant("information.prestashop.sec1.admin"),
          expanded: false,
          children: [
            {
              title: this.translate.instant(
                "information.prestashop.sec1.user1"
              ),
              icon: { icon: "child", pack: "fa" },
            },
            {
              title: this.translate.instant("information.prestashop.sec1.mdp1"),
              icon: { icon: "lock", pack: "fa" },
            },
          ],
        },

        {
          title: "Sans suppression des utilisateurs",
          expanded: false,
          children: [
            {
              title: this.translate.instant(
                "information.prestashop.sec2.user1"
              ),
              icon: { icon: "child", pack: "fa" },
            },
            {
              title: this.translate.instant("information.prestashop.sec2.mdp1"),
              icon: { icon: "lock", pack: "fa" },
            },
          ],
        },
        {
          title: this.translate.instant("information.prestashop.sec3.admin"),
          expanded: false,
          children: [
            {
              title: this.translate.instant(
                "information.prestashop.sec3.user1"
              ),
              icon: { icon: "child", pack: "fa" },
            },
            {
              title: this.translate.instant("information.prestashop.sec3.mdp1"),
              icon: { icon: "lock", pack: "fa" },
            },
          ],
        },

        {
          title: "Sans suppression des utilisateurs",
          expanded: false,
          children: [
            {
              title: this.translate.instant(
                "information.prestashop.sec4.user1"
              ),
              icon: { icon: "child", pack: "fa" },
            },
            {
              title: this.translate.instant("information.prestashop.sec4.mdp1"),
              icon: { icon: "lock", pack: "fa" },
            },
          ],
        },
      ];
    } else if (this.type === "woocommerce") {
      this.items = [
        {
          title: this.translate.instant("information.woocommerce.user1"),
          icon: { icon: "child", pack: "fa" },
        },
        {
          title: this.translate.instant("information.woocommerce.mdp1"),
          icon: { icon: "lock", pack: "fa" },
        },
      ];
    } else if (this.type === "blog") {
      this.items = [
        {
          title: this.translate.instant("information.blog.user1"),
          icon: { icon: "child", pack: "fa" },
        },
        {
          title: this.translate.instant("information.blog.mdp1"),
          icon: { icon: "lock", pack: "fa" },
        },
      ];
    } else if (this.type === "mautic") {
      this.items = [
        {
          title: this.translate.instant("information.mautic.user1"),
          icon: { icon: "child", pack: "fa" },
        },
        {
          title: this.translate.instant("information.mautic.mdp1"),
          icon: { icon: "lock", pack: "fa" },
        },
      ];
    } else if (this.type === "suitecrm") {
      this.items = [
        {
          title: this.translate.instant("information.suitecrm.user1"),
          icon: { icon: "child", pack: "fa" },
        },
        {
          title: this.translate.instant("information.suitecrm.mdp1"),
          icon: { icon: "lock", pack: "fa" },
        },
      ];
    } else if (this.type === "odoo") {
      this.items = [
        {
          title: this.translate.instant("information.odoo.sec2.profil"),
          expanded: false,
          children: [
            {
              title: this.translate.instant("information.odoo.sec2.user1"),
              icon: { icon: "child", pack: "fa" },
            },
            {
              title: this.translate.instant("information.odoo.sec2.mdp1"),
              icon: { icon: "lock", pack: "fa" },
            },
            {
              title: this.translate.instant("information.odoo.sec2.info"),
              expanded: false,
              children: [
                {
                  title: this.translate.instant("information.odoo.sec2.mdp2"),
                  icon: { icon: "lock", pack: "fa" },
                },
              ],
            },
          ],
        },
        {
          title: this.translate.instant("information.odoo.sec3.profil"),
          expanded: false,
          children: [
            {
              title: this.translate.instant("information.odoo.sec3.user1"),
              icon: { icon: "child", pack: "fa" },
            },
            {
              title: this.translate.instant("information.odoo.sec3.mdp1"),
              icon: { icon: "lock", pack: "fa" },
            },
          ],
        },
        {
          title: this.translate.instant("information.odoo.sec4.profil"),
          expanded: false,
          children: [
            {
              title: this.translate.instant("information.odoo.sec4.user1"),
              icon: { icon: "child", pack: "fa" },
            },
            {
              title: this.translate.instant("information.odoo.sec4.mdp1"),
              icon: { icon: "lock", pack: "fa" },
            },
          ],
        },
        {
          title: this.translate.instant("information.odoo.sec5.profil"),
          expanded: false,
          children: [
            {
              title: this.translate.instant("information.odoo.sec5.user1"),
              icon: { icon: "child", pack: "fa" },
            },
            {
              title: this.translate.instant("information.odoo.sec5.mdp1"),
              icon: { icon: "lock", pack: "fa" },
            },
          ],
        },
        {
          title: this.translate.instant("information.odoo.sec6.profil"),
          expanded: false,
          children: [
            {
              title: this.translate.instant("information.odoo.sec6.user1"),
              icon: { icon: "child", pack: "fa" },
            },
            {
              title: this.translate.instant("information.odoo.sec6.mdp1"),
              icon: { icon: "lock", pack: "fa" },
            },
            {
              title: this.translate.instant("information.odoo.sec6.info"),
              expanded: true,
              children: [
                {
                  title: this.translate.instant("information.odoo.sec6.mdp2"),
                  icon: { icon: "lock", pack: "fa" },
                },
              ],
            },
          ],
        },
        {
          title: this.translate.instant("information.odoo.sec7.profil"),
          expanded: false,
          children: [
            {
              title: this.translate.instant("information.odoo.sec7.user1"),
              icon: { icon: "child", pack: "fa" },
            },
            {
              title: this.translate.instant("information.odoo.sec7.mdp1"),
              icon: { icon: "lock", pack: "fa" },
            },
            {
              title: this.translate.instant("information.odoo.sec7.info"),
              expanded: true,
              children: [
                {
                  title: this.translate.instant("information.odoo.sec7.mdp2"),
                  icon: { icon: "lock", pack: "fa" },
                },
              ],
            },
          ],
        },
      ];
    } else if (this.type === "kanboard") {
      this.items = [
        {
          title: this.translate.instant("information.kanboard.user1"),
          icon: { icon: "child", pack: "fa" },
        },
        {
          title: this.translate.instant("information.kanboard.mdp1"),
          icon: { icon: "lock", pack: "fa" },
        },
      ];
    } else if (this.type === "humhub") {
      this.items = [
        {
          title: this.translate.instant("information.humhub.user1"),
          icon: { icon: "child", pack: "fa" },
        },
        {
          title: this.translate.instant("information.humhub.mdp1"),
          icon: { icon: "lock", pack: "fa" },
        },
      ];
    } else if (this.type === "sftp") {
      this.items = [
        {
          title: this.translate.instant("information.sftp.user1"),
          icon: { icon: "child", pack: "fa" },
        },
        {
          title: this.translate.instant("information.sftp.mdp1"),
          icon: { icon: "lock", pack: "fa" },
        },
      ];
    }
  }
  items = undefined;
}
