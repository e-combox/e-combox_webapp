/* eslint-disable no-invalid-this */
import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { NbDialogService } from "@nebular/theme";
import { TranslateService } from "@ngx-translate/core";
import { ToastrService } from "ngx-toastr";
import { Subscription, scheduled, queueScheduler } from "rxjs";
import { concatAll } from "rxjs/operators";
import { SERVER_TYPES } from "../../shared/api/variables";
import { ServerSelection } from "../../shared/models/server-selection";
import { Stack } from "../../shared/models/stack";
import { StackStore } from "../../shared/store/stack.store";
import { DialogNamePromptComponent } from "../dialog-name-prompt/dialog-name-prompt.component";
import { ShowcaseDialogComponent } from "../dialog-pdf/showcase-dialog.component";

interface CardSettings {
  stack: Stack;
  iconClass: string;
  on: boolean;
}

@Component({
  selector: "ecx-servers-manage",
  templateUrl: "./servers-manage.component.html",
  styleUrls: ["./servers-manage.component.scss"],
})
/**
 * Component allowing the management of the sites created
 */
export class ServersManageComponent implements OnInit, OnDestroy {
  @Input() type: string;
  @Input() custom: boolean;

  noStack = true;
  stacksList: Array<Stack> = [];
  cardsSelectedList: Array<number> = [];
  commonStatusCardsSet: Array<CardSettings> = [];
  statusCardsByThemes: {
    default: Array<CardSettings>;
  };
  private subscription: Subscription;
  checked: boolean;
  txtActions: string;
  allIsChecked = false;
  allUnchecked = false;

  /**
   *
   * @param {StackStore} stackStore
   */
  constructor(
    private stackStore: StackStore,
    private toast: ToastrService,
    private dialogService: NbDialogService,
    private translate: TranslateService
  ) {}

  /**
   *
   */
  ngOnInit(): void {
    this.txtActions = this.translate.instant("servers.actionsAll");

    this.subscription = this.stackStore.getStacks().subscribe((stacks) => {
      this.stacksList = [];
      this.commonStatusCardsSet = [];
      stacks.forEach((stack) => {
        if (stack.getServerType() && stack.getServerType() === this.type) {
          const tag = stack.getEnvVars().find((e) => e.name === "TAG")?.value;
          if (!this.custom) {
            if (tag === undefined || tag === "") {
              this.stacksList.push(stack);
            }
          } else {
            if (tag !== undefined && tag !== "") {
              this.stacksList.push(stack);
            }
          }
        }
      });
      this.stacksList.forEach((stack) => {
        // Check if stack is started
        const state: boolean = stack.getStatus() === 1;

        // Disable actions on stacks if no stack
        this.noStack = this.stacksList.length === 0;

        this.buildServerCard(stack, state);
      });
    });
  }

  /**
   * Start all existing stopped stacks
   */
  onStartAll(): void {
    if (!this.checkServersSelected) {
      return;
    }
    const requests = [];
    let msgStart = "";
    if (this.checked) {
      msgStart = "sélectionnés";
      this.cardsSelectedList.forEach((idStack) => {
        if (
          this.stacksList.find((s) => s.getID() === idStack).getStatus() === 2
        ) {
          requests.push(this.stackStore.updateStack(idStack, this.type));
        }
      });
    } else {
      msgStart = "arrêtés";
      this.stacksList.forEach((stack) => {
        if (stack.getStatus() === 2) {
          requests.push(
            this.stackStore.updateStack(stack.getID(), stack.getServerType())
          );
        }
      });
    }
    this.toast.info(`Tous les sites ${msgStart} vont être démarrés`);
    // Fix - Send requests sequentially because on some servers there is a bug with unhealthy containers
    scheduled(requests, queueScheduler)
      .pipe(concatAll())
      .subscribe(() => {
        this.checked = false;
        this.cardsSelectedList = [];
      });
  }

  /**
   * Stop all started stacks
   */
  onStopAll(): void {
    if (!this.checkServersSelected()) {
      return;
    }
    let msgStop = "";
    const requests = [];

    if (this.checked) {
      msgStop = "sélectionnés";
      this.cardsSelectedList.forEach((idStack) => {
        if (
          this.stacksList.find((s) => s.getID() === idStack).getStatus() === 1
        ) {
          requests.push(this.stackStore.stopStack(idStack));
        }
      });
    } else {
      msgStop = "démarrés";

      this.stacksList.forEach((stack) => {
        if (stack.getStatus() === 1) {
          requests.push(this.stackStore.stopStack(stack.getID()));
        }
      });
    }
    this.toast.info(`Tous les sites ${msgStop} vont être arrêtés`);
    scheduled(requests, queueScheduler)
      .pipe(concatAll())
      .subscribe(() => {
        this.checked = false;
        this.cardsSelectedList = [];
      });
  }

  /**
   * Delete all existing stacks
   */
  onDelAll(): void {
    if (!this.checkServersSelected) {
      return;
    }
    this.dialogService
      .open(DialogNamePromptComponent, {
        context: {
          message: `Cette action supprimera définitivement toutes les sites ${this.type} et les données associées`,
        },
      })
      .onClose.subscribe((name) => {
        if (name === "oui") {
          const requests = [];
          let msgStop = "";

          if (this.checked) {
            msgStop = "sélectionnés";
            this.cardsSelectedList.forEach((idStack) => {
              requests.push(
                this.stackStore.deleteStack(
                  idStack,
                  this.stacksList.find((s) => s.getID() === idStack).getName()
                )
              );
            });
          } else {
            this.stacksList.forEach((stack) => {
              requests.push(
                this.stackStore.deleteStack(stack.getID(), stack.getName())
              );
            });
          }
          this.toast.info(`Tous les sites ${msgStop} vont être supprimés`);
          scheduled(requests, queueScheduler)
            .pipe(concatAll())
            .subscribe(() => {
              this.checked = false;
              this.cardsSelectedList = [];
              if (this.stacksList.length === 0) this.noStack = true;
            });
        }
      });
  }

  /**
   * Check if servers are be selected. Return true if there is the case, false instead.
   * @return {boolean}
   */
  checkServersSelected(): boolean {
    if (this.cardsSelectedList.length > 0) {
      return true;
    } else {
      this.toast.info(
        `Veuillez sélectionner au moins un site pour pouvoir effectuer une action.`
      );
      return false;
    }
  }

  /**
   *
   */
  onSelectedChange(): void {
    if (this.checked) {
      this.txtActions = this.translate.instant("servers.actionsSome");
    } else {
      this.txtActions = this.translate.instant("servers.actionsAll");
      this.cardsSelectedList = [];
    }
  }

  /**
   *
   * @param {ServerSelection} $event
   */
  onCardSelected($event: ServerSelection): void {
    if ($event.selected) {
      this.cardsSelectedList.push($event.idStack);
    } else {
      this.cardsSelectedList.splice(
        this.cardsSelectedList.indexOf($event.idStack),
        1
      );
      if (this.allIsChecked) {
        this.allIsChecked = false;
      }
    }
  }

  /**
   * Generate PDF file with site URLs
   */
  onGetPdf(): void {
    const listURL = [];
    let passwd = "";
    this.stacksList.forEach((stack) => {
      if (stack.getStatus() == 1) {
        if (this.type === SERVER_TYPES.mautic) {
          passwd = stack.getEnvVars().find((e) => e.name === "DB_PASS").value;
        }

        listURL.push({
          site: stack.getMainContainer().getName(),
          url: stack.getMainContainer().getUrlBackOffice(),
          mdpMautic: passwd,
        });
      }
    });
    this.dialogService.open(ShowcaseDialogComponent, {
      context: {
        title: "Liste des sites " + this.type + " avec leur URL",
        listURL: listURL,
        typeSite: this.type,
      },
    });
  }

  /**
   *
   * @param {Stack} stack
   * @param {boolean} state
   * @param {string} serverType
   */
  buildServerCard(stack: Stack, state: boolean): void {
    const serverCard: CardSettings = {
      stack: stack,
      iconClass: "nb-power-circled",
      on: state,
    };

    this.commonStatusCardsSet.push(serverCard);
  }

  /**
   *
   * @param {boolean} checked
   */
  toggle(checked: boolean): void {
    this.allIsChecked = checked;
    this.allUnchecked = !checked;
  }

  /**
   *
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
