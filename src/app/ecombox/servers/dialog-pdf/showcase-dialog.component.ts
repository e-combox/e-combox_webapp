import { Component, Input } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";
import { IMAGE } from "../../shared/api/variables";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: "ngx-showcase-dialog",
  templateUrl: "showcase-dialog.component.html",
  styleUrls: ["showcase-dialog.component.scss"],
})
/**
 * Dialog box allowing the generation of URLs of a site in the form of a PDF file
 */
export class ShowcaseDialogComponent {
  @Input() title: string;
  @Input() listURL = [];
  @Input() typeSite: string;
  @Input() isMautic = false;

  /**
   *
   * @param {NbDialogRef} ref
   */
  constructor(protected ref: NbDialogRef<ShowcaseDialogComponent>) {}

  /**
   *
   */
  ngOnInit(): void {
    if (this.typeSite === "mautic") {
      this.isMautic = true;
    }
  }

  /**
   *
   */
  export(): void {
    const docDefinition = {
      info: {
        title: "Liste des URL " + this.typeSite,
        author: "e-comBox",
      },
      content: [
        {
          width: 50,
          height: 50,
          image: IMAGE,
        },
        {
          text:
            "Liste des sites " +
            this.typeSite +
            " avec les URL correspondantes \n\n\n",
          bold: true,
          fontSize: 15,
          alignment: "center",
        },
        {
          stack: [
            {
              text: [
                { text: "", bold: true },
                { text: "", link: "" },
                { text: "", bold: true },
              ],
            },
          ],
          margin: [0, 5, 0, 0],
          alignment: "justify",
        },
      ],
    };

    if (this.isMautic) {
      this.listURL.forEach((unSite) =>
        docDefinition.content.push({
          stack: [
            {
              text: [
                { text: unSite.site + " : ", bold: true },
                { text: unSite.url + " ", link: unSite.url },
                { text: " - (MDP : " + unSite.mdpMautic + ")", bold: true },
              ],
            },
          ],
          margin: [0, 5, 0, 0],
          alignment: "justify",
        })
      );
    } else {
      this.listURL.forEach((unSite) =>
        docDefinition.content.push({
          stack: [
            {
              text: [
                { text: unSite.site + " : ", bold: true },
                { text: unSite.url + " ", link: unSite.url },
                { text: "", bold: true },
              ],
            },
          ],
          margin: [0, 5, 0, 0],
          alignment: "justify",
        })
      );
    }

    pdfMake.createPdf(docDefinition).open();
    // pdfMake.createPdf(docDefinition).download();
  }

  /**
   * Hide dialog pdf
   */
  dismiss(): void {
    this.ref.close();
  }
}
