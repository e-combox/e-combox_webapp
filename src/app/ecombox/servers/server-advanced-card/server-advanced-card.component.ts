import { HttpErrorResponse } from "@angular/common/http";
import { Component, Input } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Stack } from "../../shared/models/stack";
import { StackStore } from "../../shared/store/stack.store";
@Component({
  selector: "ecx-server-advanced-card",
  styleUrls: ["./server-advanced-card.component.scss"],
  templateUrl: "./server-advanced-card.component.html",
})
/**
 * Cards structure of SFTP and PMA type sites
 */
export class ServerAdvancedCardComponent {
  @Input() title: string;
  @Input() type: string;
  @Input() id: string;
  @Input() on: boolean;
  @Input() url: string;
  @Input() password: string;
  @Input() typeContainer: string;
  @Input() nameStackAss: string;
  @Input() actif: string;
  @Input() idStackAss: number;
  @Input() typeStackAss: string;
  @Input() hostSftp: string;

  loading = false;

  /**
   * @param {ToastrService} toastr
   */
  constructor(private toastr: ToastrService, private stackStore: StackStore) {
    this.toastr.toastrConfig.timeOut = 5000;
    this.on = true;
  }

  /**
   *
   */
  public onClick(): void {
    this.loading = true;
    if (!this.on) {
      this.startStack(this.stackStore.getStack(Number.parseInt(this.id)));
    } else {
      this.stopStack();
    }
  }

  /**
   *
   * @param {Stack} stackMain
   */
  private startStack(stackMain: Stack): void {
    this.stackStore.createStack(this.typeStackAss, stackMain).subscribe(
      (result) => {
        this.loading = false;
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
        console.error(error.error.details);
      }
    );
  }

  /**
   *
   */
  private stopStack(): void {
    this.stackStore.deleteStack(this.idStackAss, this.nameStackAss).subscribe(
      (result) => {
        this.loading = false;
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
        console.error(error.error.details);
      }
    );
  }
}
