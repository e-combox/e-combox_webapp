import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

import { ServerBaseComponent } from "./server-base/server-base.component";
import { ServerAdvancedComponent } from "./server-advanced/server-advanced.component";

const routes: Routes = [
  { path: "prestashop", component: ServerBaseComponent },
  { path: "woocommerce", component: ServerBaseComponent },
  { path: "blog", component: ServerBaseComponent },
  { path: "mautic", component: ServerBaseComponent },
  { path: "humhub", component: ServerBaseComponent },
  { path: "kanboard", component: ServerBaseComponent },
  { path: "odoo", component: ServerBaseComponent },
  { path: "suitecrm", component: ServerBaseComponent },
  { path: "pma", component: ServerAdvancedComponent },
  { path: "sftp", component: ServerAdvancedComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServersRoutingModule {}
