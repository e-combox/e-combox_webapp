import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { SERVER_TYPES } from "../../shared/api/variables";
import { Stack } from "../../shared/models/stack";
import { StackStore } from "../../shared/store/stack.store";

interface CardSettings {
  title: string;
  iconClass: string;
  id: string;
  type: string;
  on: boolean;
  actif: string;
  url: string;
  typeContainer: string;
  nameStackAss: string;
  idStackAss: number;
  typeStackAss: string;
  password: string;
  hostSftp?: string;
}

interface ControlsMap<T extends Array<CardSettings>> {
  [key: string]: T;
}

@Component({
  selector: "ecx-server-advanced",
  templateUrl: "./server-advanced.component.html",
  styleUrls: ["./server-advanced.component.scss"],
})
/**
 * Basic component used to display the elements necessary for the creation and management of PMA or SFTP type sites
 */
export class ServerAdvancedComponent implements OnInit, OnDestroy {
  serverType: string;
  title: string;
  warning: string;
  sftpClient: string;
  display: boolean;
  loading: false;
  test: ControlsMap<Array<CardSettings>> = {};
  domain: string;

  stacksList: Array<Stack> = [];
  commonStatusCardsSet: Array<CardSettings> = [];
  statusCardsByThemes: {
    default: Array<CardSettings>;
  };
  private subscription: Subscription;

  /**
   *
   * @param {StackStore} stackStore
   */
  constructor(
    private stackStore: StackStore,
    private actRoute: ActivatedRoute
  ) {
    this.serverType = this.actRoute.snapshot.url.toString();
  }

  /**
   *
   */
  ngOnInit(): void {
    if (this.serverType === SERVER_TYPES.pma) {
      this.title = "Gestion de l'accès à phpMyAdmin (PMA)";
      this.warning = `Les sites listés sur cette page sont les sites Prestashop, WooCommerce et Blog actuellement démarrés
	   car cela est obligatoire pour pouvoir activer phpMyAdmin.`;
    } else if (this.serverType === SERVER_TYPES.sftp) {
      this.title = "Gestion du SFTP";
      this.warning = `Les sites listés sur cette page sont les sites Prestashop et Odoo actuellement démarrés 
		car cela est obligatoire pour pouvoir activer le SFTP et accéder aux sources.`;
    }

    this.subscription = this.stackStore.getStacks().subscribe((stacks) => {
      this.stacksList = [];
      this.test = {};
      stacks.forEach((stack) => {
        switch (this.serverType) {
          case SERVER_TYPES.sftp:
            if (
              stack.getServerType() &&
              (stack.getServerType() === SERVER_TYPES.prestashop ||
                stack.getServerType() === SERVER_TYPES.odoo) &&
              stack.getStatus() === 1
            ) {
              this.displayCards(stack);
            }
            break;
          case SERVER_TYPES.pma:
            if (
              stack.getStatus() === 1 &&
              stack.getServerType() &&
              (stack.getServerType() === SERVER_TYPES.prestashop ||
                stack.getServerType() === SERVER_TYPES.woocommerce ||
                stack.getServerType() === SERVER_TYPES.blog)
            ) {
              this.displayCards(stack);
            }
            break;
        }
      });
    });
  }

  /**
   * @param {Stack} stack
   */
  private displayCards(stack: Stack): void {
    let state = false;
    let info = "";
    let idAss: number;
    let nameAss = "";
    let passwd = "";
    let port: number;
    // We check if an associate sftp or pma stack exists
    const stackAssociate = this.stackStore.getStackAssociate(stack.getName());

    if (
      stackAssociate.length > 0 &&
      stackAssociate[0].getContainers() !== undefined
    ) {
      stackAssociate.forEach((s) => {
        if (
          this.serverType === SERVER_TYPES.sftp &&
          s.getName().startsWith("sftp")
        ) {
          state = true;
          info = this.getConnectInfo(
            s.getContainers()[0].getPort(),
            stack.getName()
          );
          nameAss = s.getName();
          idAss = s.getID();
          passwd = s.getEnvVars().find((e) => e.name === "SFTP_USER_PASS")
            .value;
          port = s.getContainers()[0].getPort();
          return;
        } else if (
          this.serverType === SERVER_TYPES.pma &&
          s.getName().startsWith("pma")
        ) {
          state = true;

          info = this.getConnectInfo(port, stack.getName());
          nameAss = s.getName();
          idAss = s.getID();
          return;
        }
      });
    }

    // We get suffix of the main stack
    let suffix: string;
    if (stack?.getEnvVars() !== null && stack?.getEnvVars() !== undefined) {
      suffix = stack?.getEnvVars().find((e) => e.name === "SUFFIXE").value;
    }
    this.buildServerCard(
      stack.getServerType() + "-" + suffix,
      stack.getID(),
      state,
      nameAss,
      // CSS style for stack associate status
      state ? "active" : "desactive",
      stack.getServerType(),
      info,
      idAss,
      this.serverType,
      passwd,
      port
    );
  }

  /**
   *
   * @param {number} port
   * @param {string} name - Name of main stack
   * @param {string} passwd Password for SFTP containers
   * @return {string}
   */
  public getConnectInfo(port: number, name: string, passwd?: string): string {
    let info = "";
    if (
      this.stackStore.getPublicURL() !== "" &&
      this.stackStore.getPublicURL() !== undefined
    ) {
      this.domain = this.stackStore.getPublicURL();
    } else {
      this.domain = this.stackStore.getDockerIP();
    }
    switch (this.serverType) {
      case SERVER_TYPES.pma:
        if (this.stackStore.getPath() !== "") {
          info = `https://${this.stackStore.getFQDN()}/pma-${name}/`;
        } else {
          info = `https://${
            this.domain
          }:${this.stackStore.getNginxPort()}/pma-${name}/`;
        }
        break;
      case SERVER_TYPES.sftp:
        if (this.stackStore.getPath() !== "") {
          info = `https://${this.stackStore.getFQDN()}/sftp-${name}/`;
        } else {
          info = `https://${
            this.domain
          }:${this.stackStore.getNginxPort()}/sftp-${name}/`;
        }
        break;
    }
    return info;
  }

  /**
   *
   * @param {string} title
   * @param {string} stackID
   * @param {boolean} state
   * @param {string} assName
   * @param {string} active
   * @param {string} mainStackType
   * @param {string} info
   * @param {number} idStackAss
   * @param {string} typeStackAss
   * @param {string} passwd
   * @param {number} port
   */
  buildServerCard(
    title: string,
    stackID: number,
    state: boolean,
    assName: string,
    active: string,
    mainStackType: string,
    info: string,
    idStackAss: number,
    typeStackAss: string,
    passwd: string,
    port: number
  ): void {
    let icon: string;
    if (typeStackAss === SERVER_TYPES.pma) {
      icon = "nb-power-circled";
    } else if (typeStackAss === SERVER_TYPES.sftp) {
      icon = "nb-angle-double-right";
    }
    const serverCard: CardSettings = {
      title: title,
      iconClass: icon,
      type: "success",
      on: state,
      actif: active,
      id: stackID.toString(),
      nameStackAss: assName,
      typeContainer: mainStackType,
      url: info,
      idStackAss: idStackAss,
      typeStackAss: typeStackAss,
      password: passwd,
      hostSftp: this.domain + ":" + port,
    };

    if (this.test[mainStackType] === undefined) {
      this.test[mainStackType] = [];
    }
    this.test[mainStackType].push(serverCard);
  }

  /**
   *
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
