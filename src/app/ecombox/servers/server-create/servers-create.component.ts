import { HttpErrorResponse } from "@angular/common/http";
import { Component, Input, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { NbDialogService, NbPopoverDirective } from "@nebular/theme";
import { TranslateService } from "@ngx-translate/core";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { DockerRegistryService } from "../../shared/api/docker-registry.service";
import {
  SERVER_TYPES,
  PARAM_OPTION,
  PARAM_STACK_NAME,
  GET_PARAM_STACK,
} from "../../shared/api/variables";
import { StackStore } from "../../shared/store/stack.store";
import { DialogMultipleStacksPromptComponent } from "../dialog-multiple-stacks-prompt/dialog-multiple-stacks-prompt.component";

@Component({
  selector: "ecx-servers-create",
  templateUrl: "./servers-create.component.html",
  styleUrls: ["./servers-create.component.scss"],
})
/**
 * Component allowing the creation of a new site
 */
export class ServersCreateComponent implements OnInit, OnDestroy {
  selectedOption = "certa";
  extension = "";
  dbType = "";
  message = "";
  suffixReset = "";

  toolTipModel = "";
  toolTipModelName = "";
  toolTipName = "";

  loading = false;
  customDisabled = false;
  imagesList = [];
  imagesListCerta = [];
  imagesListCustom = [];
  image: { label: string; value: string };
  selectedTemplate: string;
  private subscription: Subscription;

  @Input() type: string;
  // eslint-disable-next-line new-cap
  @ViewChild(NbPopoverDirective) popover: NbPopoverDirective;

  /**
   * Constructor
   * @param {TranslateService} translate
   */
  constructor(
    private translate: TranslateService,
    public stackStore: StackStore,
    private registryService: DockerRegistryService,
    private dialogService: NbDialogService,
    private toastr: ToastrService
  ) {
    this.toolTipModel =
      "Choisissez de créer un site à partir d'un modèle fourni par l'équipe de l'e-comBox ou bien à partir d'un modèle créé par vos soins";
    this.toolTipModelName = "Choisissez le modèle à utiliser";
    this.toolTipName = "Personnalisez le nom de votre futur site";
  }

  /**
   * @return {void}
   */
  ngOnInit(): void {
    let params = null;
    // @TODO - refactor
    if (this.type === SERVER_TYPES.odoo) {
      params = [
        [PARAM_OPTION.v12, "servers.odoo.v12"],
        [PARAM_OPTION.v13, "servers.odoo.v13"],
        [PARAM_OPTION.v14, "servers.odoo.v14"],
        [PARAM_OPTION.v15, "servers.odoo.v15"],
        [PARAM_OPTION.v16, "servers.odoo.v16"],
        [PARAM_OPTION.ada, "servers.odoo.ada"],
        [PARAM_OPTION.generik, "servers.odoo.generik"],
        [PARAM_OPTION.pepinieres, "servers.odoo.pepinieres"],
        [PARAM_OPTION.primeur, "servers.odoo.primeur"],
        [PARAM_OPTION.surplomb, "servers.odoo.surplomb"],
        [PARAM_OPTION.sweetybio, "servers.odoo.sweetybio"],
      ];
    } else if (this.type === SERVER_TYPES.woocommerce) {
      params = [
        [PARAM_OPTION.vierge, "servers.woocommerce.empty"],
        [PARAM_OPTION.perso, "servers.woocommerce.art"],
      ];
    } else if (this.type === SERVER_TYPES.prestashop) {
      params = [
        [PARAM_OPTION.vierge, "servers.prestashop.empty"],
        [PARAM_OPTION.perso, "servers.prestashop.art"],
      ];
    } else if (this.type === SERVER_TYPES.kanboard) {
      params = [
        [PARAM_OPTION.vierge, "servers.kanboard.empty"],
        [PARAM_OPTION.bddev, "servers.kanboard.bddev"],
      ];
    }
    this.initOptions(params);

    this.subscription = this.stackStore.messageSource.subscribe(
      (value: string) => {
        if (value === "update") {
          this.imagesListCustom = [];
          let domain = "";
          let port: string = null;
          if (this.stackStore.getPath() !== "") {
            domain = this.stackStore.getFQDN();
          } else {
            if (
              this.stackStore.getPublicURL() !== "" &&
              this.stackStore.getPublicURL() !== undefined
            ) {
              domain = this.stackStore.getPublicURL();
            } else {
              domain = this.stackStore.getDockerIP();
            }
            port = this.stackStore.getNginxPort();
          }
          this.registryService.registryTags(this.type, domain, port).subscribe(
            (result) => {
              if (result.tags !== null) {
                this.customDisabled = false;
                result.tags.forEach((element) => {
                  this.imagesListCustom.push({
                    value: element,
                    label: element,
                  });
                });

                this.changeList();
              } else {
                this.customDisabled = true;
              }
            },
            (error: HttpErrorResponse) => {
              this.customDisabled = true;
            }
          );
        }
      }
    );
  }

  /**
   *
   */
  resetForm(): void {
    this.dbType = "";
    this.extension = "";
    this.selectedTemplate = this.imagesList[0];
    this.selectedOption = "certa";
    this.suffixReset = null;
    this.changeList();
    this.close();
  }

  /**
   *
   */
  open(): void {
    this.popover.show();
  }

  /**
   *
   */
  close(): void {
    this.popover.hide();
  }

  /**
   * Initialize images list
   * @param {string} params
   */
  initOptions(params: string[][]): void {
    if (params !== null) {
      let label = "";
      for (let i = 0; i < params.length; i++) {
        this.translate.get(params[i][1]).subscribe((res: string) => {
          label = res;
        });
        this.imagesListCerta.push({ value: params[i][0], label: label });
      }
    }
    this.imagesList = this.imagesListCerta;
  }

  /**
   *
   */
  changeList(): void {
    this.dbType = "";
    this.extension = "";
    if (this.selectedOption === "certa") {
      this.imagesList = this.imagesListCerta;
    } else {
      this.imagesList = this.imagesListCustom;
    }
    this.selectedTemplate = this.imagesList[0];
  }

  /**
   * Get values from radio button
   * @param {string} value
   */
  onValueChanged(): void {
    this.changeList();
  }

  /**
   * Get values from select list
   * @param {string} value
   */
  onSelectChanged(value: string): void {
    if (this.selectedOption === "certa") {
      this.extension = PARAM_STACK_NAME[value];
      this.dbType = PARAM_OPTION[value];
    } else {
      this.extension = value + "-";
    }
  }

  /**
   * Check some parameters to confirm creation
   * @param {string} suffix Server name
   */
  onValidClick(suffix: string): void {
    let message = "";
    if (GET_PARAM_STACK.repositoryURL.startsWith("https:///")) {
      this.translate.get("servers.git_error").subscribe((res: string) => {
        message = res;
      });
      this.toastr.error(`Un problème est survenu : ${message}`);
      return;
    }
    this.launchCreationStack(suffix);
  }

  /**
   * To create a docker container
   * @param {string} suffix Server name
   */
  launchCreationStack(suffix: string): void {
    // Checking for keyup enter on create screen or on dialog to confirm creation
    if (suffix.length > 0) {
      let customImage = false;
      if (this.selectedOption === "custom") {
        customImage = true;
        if (
          this.selectedTemplate === undefined ||
          this.selectedTemplate === ""
        ) {
          this.message = "Veuillez sélectionner un modèle de site.";
          this.open();
          return;
        }
      } else {
        if (
          this.imagesListCerta.length > 0 &&
          (this.selectedTemplate === undefined || this.selectedTemplate === "")
        ) {
          this.message = "Veuillez sélectionner un modèle de site.";
          this.open();
          return;
        }
      }

      const regex = RegExp("^[a-z0-9]+$");
      if (!regex.test(suffix)) {
        this.message =
          "Le suffixe ne peut contenir que des minuscules et des chiffres";
        this.open();
        return;
      }

      if (this.dbType === "") {
        this.dbType = PARAM_OPTION["other"];
      }

      this.close();

      this.dialogService
        .open(DialogMultipleStacksPromptComponent)
        .onClose.subscribe((nb) => {
          if (nb !== undefined) {
            this.stackStore.createStackSequentiallyStacks(
              this.type,
              this.dbType,
              suffix,
              customImage,
              this.selectedTemplate,
              nb
            );
          }
          this.resetForm();
        });
    }
  }

  /**
   *
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
