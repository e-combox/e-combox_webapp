import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { SystemService } from "../../shared/api/system.service";
import { EcbInfo } from "../../shared/models/ecb-info";

@Component({
  selector: "ngx-server-base",
  styleUrls: ["./server-base.component.scss"],
  templateUrl: "./server-base.component.html",
})
/**
 * Basic component used to display the elements necessary for the creation and management of sites (business applications)
 */
export class ServerBaseComponent implements OnInit {
  serverType: string;
  message: string;
  isDisplaying = false;

  /**
   * Constructor
   * @param {ActivatedRoute} actRoute
   * @param {TranslateService} translate
   */
  constructor(
    private actRoute: ActivatedRoute,
    private systemService: SystemService
  ) {
    this.serverType = this.actRoute.snapshot.url.toString();
  }

  /**
   *
   */
  ngOnInit(): void {
    if (
      !localStorage.getItem("ecombox.notification.display") ||
      (localStorage.getItem("ecombox.notification.display") === "true" &&
        localStorage.getItem("ecombox.notification.server") ===
          this.serverType) ||
      localStorage.getItem("ecombox.notification.server") === "all"
    ) {
      this.systemService.searchUpdate().subscribe(
        (response) => {
          this.displayNotification(response);
        },
        (error: Error) => {
          console.error(error.message);
        }
      );
    }
  }

  /**
   *
   * @param {EcbInfo} notification
   */
  private displayNotification(notification: EcbInfo): void {
    if (notification.displayNotification) {
      this.isDisplaying = notification.displayNotification;
      this.message = notification.messageNotification;
      localStorage.setItem("ecombox.notification.display", "true");
      localStorage.setItem(
        "ecombox.notification.server",
        notification.serverTypeNotification
      );
    } else {
      localStorage.setItem("ecombox.notification.display", "false");
      localStorage.removeItem("ecombox.notification.server");
    }
  }

  /**
   *
   */
  onClose(): void {
    this.isDisplaying = false;
    localStorage.setItem("ecombox.notification.display", "false");
    localStorage.removeItem("ecombox.notification.server");
  }
}
