import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { SystemService } from "../../shared/api/system.service";

@Component({
  selector: "ngx-server-base",
  styleUrls: ["./server-base.component.scss"],
  templateUrl: "./server-base.component.html",
})
/**
 * Basic component used to display the elements necessary for the creation and management of sites (business applications)
 */
export class ServerBaseComponent implements OnInit {
  serverType: string;
  message: string;
  isDisplaying = false;

  /**
   * Constructor
   * @param {ActivatedRoute} actRoute
   * @param {TranslateService} translate
   */
  constructor(
    private actRoute: ActivatedRoute,
    private systemService: SystemService
  ) {
    this.serverType = this.actRoute.snapshot.url.toString();
  }

  /**
   *
   */
  ngOnInit(): void {
    if (
      !localStorage.getItem("ecombox.notification.display") ||
      (localStorage.getItem("ecombox.notification.display") === "true" &&
        localStorage.getItem("ecombox.notification.server") ===
          this.serverType) ||
      localStorage.getItem("ecombox.notification.server") === "all"
    ) {
      this.systemService.searchUpdate().subscribe(
        (response) => {
          if (response?.serverNotification?.display) {
            this.isDisplaying = response?.serverNotification?.display;
            this.message = response?.serverNotification?.message;
            localStorage.setItem("ecombox.notification.display", "true");
            localStorage.setItem(
              "ecombox.notification.server",
              response?.serverNotification?.serverType
            );
          } else {
            localStorage.setItem("ecombox.notification.display", "false");
            localStorage.removeItem("ecombox.notification.server");
          }
        },
        (error: Error) => {
          console.error(error.message);
        }
      );
    }
  }

  /**
   *
   */
  onClose(): void {
    this.isDisplaying = false;
    localStorage.setItem("ecombox.notification.display", "false");
    localStorage.removeItem("ecombox.notification.server");
  }
}
