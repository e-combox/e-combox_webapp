/* eslint-disable new-cap */
import { Container } from "../../shared/models/container";
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from "@angular/core";
import { Stack } from "../../shared/models/stack";
import { StackStore } from "../../shared/store/stack.store";
import { NbDialogService } from "@nebular/theme";
import { DialogNamePromptComponent } from "../dialog-name-prompt/dialog-name-prompt.component";
import { SERVER_TYPES } from "../../shared/api/variables";
import { DialogTemplateComponent } from "../dialog-template/dialog-template.component";
import { HttpErrorResponse } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { ServerSelection } from "../../shared/models/server-selection";

@Component({
  selector: "ecx-server-status-card",
  templateUrl: "./server-status-card.component.html",
  styleUrls: ["./server-status-card.component.scss"],
})
/**
 * Structure of site cards. Also allows the management of created sites.
 */
export class ServerStatusCardComponent implements OnInit, OnChanges {
  items = [
    { title: "Arrêter/Démarrer" },
    { title: "Supprimer" },
    { title: "Dupliquer" },
  ];

  @Input() stack: Stack;
  @Input() isSelected: boolean;
  @Input() allSelected: boolean;
  @Input() allUnchecked: boolean;
  @Output() cardSelect: EventEmitter<ServerSelection> = new EventEmitter<
    ServerSelection
  >();

  isOn: boolean;
  serverType: string;
  mainContainer: Container;
  loading = false;
  urlBack = "";
  urlFront = "";
  valid: string;
  password = "";
  tag = "";
  title = "";
  cssCardSelected: string;
  cssCard: string;
  isCardSelected: boolean;
  stacksWithFront = [
    SERVER_TYPES.blog,
    SERVER_TYPES.prestashop,
    SERVER_TYPES.woocommerce,
  ];

  /**
   *
   */
  constructor(
    private store: StackStore,
    private dialogService: NbDialogService,
    private toastr: ToastrService
  ) {
    this.toastr.toastrConfig.timeOut = 10000;
  }

  /**
   *
   */
  ngOnInit(): void {
    this.serverType = this.stack.getServerType();

    this.cssCardSelected = "";
    this.isCardSelected = false;

    if (this.stack.getStatus() === 1) {
      this.isOn = true;
      this.cssCard = "";
      this.mainContainer = this.stack.getMainContainer();
      if (this.mainContainer !== undefined) {
        this.urlBack = this.mainContainer.getUrlBackOffice();
        this.urlFront = this.mainContainer.getUrlFrontOffice();
        this.title = this.mainContainer.getName();
        if (this.serverType === SERVER_TYPES.mautic) {
          // Display password for mautic servers
          this.password = this.stack
            .getEnvVars()
            .find((e) => e.name === "DB_PASS").value;
        }
      }
    } else {
      this.isOn = false;
      this.cssCard = "off";
      this.title = this.stack.getServerType() + "-" + this.stack.getSuffix();
    }
    this.tag = this.stack.getEnvVars().find((e) => e.name === "TAG")?.value;
  }

  /**
   *
   * @param {SimpleChanges} changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (!this.isSelected) {
      this.cssCardSelected = "";
      this.isCardSelected = false;
    } else {
      if (this.allSelected && !this.isCardSelected) {
        this.onSelectCard();
      } else if (this.allUnchecked) {
        this.onSelectCard();
      }
    }
  }

  /**
   *
   */
  onClick(): void {
    if (!this.isSelected) {
      this.loading = true;
      if (this.isOn) {
        this.stop();
      } else {
        this.start();
      }
    }
  }

  /**
   *
   */
  confirmDel(): void {
    this.dialogService
      .open(DialogNamePromptComponent, {
        context: {
          message:
            "Cette action supprimera définitivement toutes les données associées à ce site",
        },
      })
      .onClose.subscribe((name) => {
        this.valid = name;
        if (name === "oui") {
          this.loading = true;
          this.store
            .deleteStack(this.stack.getID(), this.stack.getName())
            .subscribe(
              () => {
                this.loading = false;
              },
              (error: HttpErrorResponse) => {
                this.loading = false;
                if (error.status === 404) {
                  this.toastr.warning(
                    `Une erreur est survenue, veuillez retenter l'opération dans quelques minutes ou utiliser l'accès admin`
                  );
                }
              }
            );
          this.store.deleteLinkedStack(this.stack);
        }
      });
  }

  /**
   * @TODO
   */
  onDuplicate(): void {
    this.dialogService.open(DialogTemplateComponent, {
      context: {
        stack: this.stack,
      },
      closeOnBackdropClick: false,
    });
  }

  /**
   *
   */
  onReset(): void {
    this.dialogService
      .open(DialogNamePromptComponent, {
        context: {
          message:
            "Attention, cetta action supprimera toutes les modifications effectuées sur ce site.",
        },
      })
      .onClose.subscribe((name) => {
        this.valid = name;
        if (name === "oui") {
          this.loading = true;
          this.store.resetStack(this.stack, this.tag).subscribe(
            () => {
              // 2 seconds of delay for nginx
              setTimeout(() => {
                this.loading = false;
              }, 2000);
            },
            (error: Error) => {
              console.error(error.message);
              this.loading = false;
            }
          );
        }
      });
  }

  /**
   *
   */
  private start(): void {
    this.store.updateStack(this.stack.getID(), this.serverType).subscribe(
      () => {
        // 2 seconds of delay for nginx
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      },
      (error: Error) => {
        console.error(error.message);
        this.loading = false;
      }
    );
  }

  /**
   *
   */
  private stop(): void {
    this.store.stopStack(this.stack.getID()).subscribe(() => {
      this.loading = false;
    });
  }

  /**
   *
   */
  onSelectCard(): void {
    if (this.isSelected) {
      this.isCardSelected = !this.isCardSelected;
      this.cssCardSelected = this.isCardSelected ? " selected" : "";

      this.cardSelect.emit({
        idStack: this.stack.getID(),
        selected: this.isCardSelected,
      });
    }
  }
}
