import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { ImageService } from "../../shared/api/image.service";
import { StackStore } from "../../shared/store/stack.store";
import { DockerRegistryService } from "../../shared/api/docker-registry.service";
import { IMPORT_SITE } from "../../shared/api/variables";
import { ToastrService } from "ngx-toastr";
import { HttpErrorResponse } from "@angular/common/http";
import { TranslateService } from "@ngx-translate/core";
import { mergeMap, concatMap, catchError } from "rxjs/operators";
import { Observable, empty } from "rxjs";

@Component({
  selector: "ecx-images-import-form",
  templateUrl: "./images-import-form.component.html",
  styleUrls: ["./images-import-form.component.scss"],
})
export class ImagesImportFormComponent {
  imagesList = [];
  image: string;
  username = "";
  siteType = "";
  modelName = "";
  serverList = [
    "prestashop",
    "woocommerce",
    "blog",
    "mautic",
    "suitecrm",
    "odoo",
    "kanboard",
    "humhub",
  ];

  /**
   * Constructor
   * @param {Router} router
   * @param {HttpClient} httpClient
   * @param {ImageService} imageService
   * @param {StackStore} store
   * @param {DockerRegistryService} registryService
   * @param {ToastrService} toastr
   * @param {TranslateService} translate
   */
  constructor(
    private router: Router,
    protected httpClient: HttpClient,
    private imageService: ImageService,
    private store: StackStore,
    private registryService: DockerRegistryService,
    private toastr: ToastrService,
    public translate: TranslateService
  ) {}

  /**
   *
   * @param {string} value
   */
  usernameChanged(value: string): void {
    IMPORT_SITE.imagesList = [];
    this.username = encodeURIComponent(value);
    if (this.siteType.length > 0) {
      this.imageService.getlisttags(this.username, this.siteType).subscribe(
        (res) => {
          for (let i = 0; i < res.count; i++) {
            IMPORT_SITE.imagesList.push(res.results[i].name);
          }
        },
        (error: HttpErrorResponse) => {
          if (error.status === 404) {
            IMPORT_SITE.imagesList.push(
              this.translate.instant("message.exist1") +
                this.siteType +
                this.translate.instant("message.exist2")
            );
          } else {
            console.dir(error.error);
          }
        }
      );
      this.imagesList = IMPORT_SITE.imagesList;
    }
  }

  /**
   *
   * @param {string} value
   */
  typeChanged(value: string): void {
    IMPORT_SITE.imagesList = [];
    this.siteType = value;
    if (this.username.length > 0) {
      this.imageService.getlisttags(this.username, this.siteType).subscribe(
        (res) => {
          for (let i = 0; i < res.count; i++) {
            IMPORT_SITE.imagesList.push(res.results[i].name);
          }
        },
        (error: HttpErrorResponse) => {
          if (error.status === 404) {
            IMPORT_SITE.imagesList.push(
              this.translate.instant("message.exist1") +
                this.siteType +
                this.translate.instant("message.exist2")
            );
          } else {
            console.dir(error.error);
          }
        }
      );
      this.imagesList = IMPORT_SITE.imagesList;
    }
  }

  /**
   *
   * @param {string} value
   */
  nameChanged(value: string): void {
    this.modelName = value;
  }

  /**
   *
   */
  import(): void {
    const [domain, port] = this.imageService.urlBasePath();
    const localPort = this.store.getResgistryPort();
    const oldName = this.imageService.imageExportName(
      this.username,
      this.siteType,
      this.modelName
    );
    const oldNamedb = this.imageService.imageExportName(
      this.username,
      this.siteType + this.translate.instant("extension"),
      this.modelName
    );
    let newName = this.imageService.imageLocalName(
      localPort,
      this.siteType,
      this.modelName
    );
    let newNamedb = this.imageService.imageLocalName(
      localPort,
      this.siteType + this.translate.instant("extension"),
      this.modelName
    );

    this.registryService
      .registryTags(this.siteType, domain, port)
      .pipe(
        mergeMap((res) => {
          let exist = false;
          if (res.tags) {
            exist = res.tags.includes(this.modelName);
          }
          if (exist) {
            return this.imageService.onSameName(res).pipe(
              mergeMap((rep) => {
                if (rep[0]) {
                  newName = this.imageService.imageLocalName(
                    localPort,
                    this.siteType,
                    rep[1]
                  );
                  newNamedb = this.imageService.imageLocalName(
                    localPort,
                    this.siteType + this.translate.instant("extension"),
                    rep[1]
                  );
                  this.modelName = rep[1];
                }
                if (rep.length > 0) {
                  this.router.navigate(["ecombox/images"]);
                  this.toastr.info(
                    "L'import" + this.translate.instant("message.attente")
                  );
                  return this.imageService
                    .imageShareUpDate(
                      oldNamedb,
                      newNamedb,
                      "",
                      "",
                      this.siteType + "-db",
                      domain,
                      port
                    )
                    .pipe(
                      mergeMap(() =>
                        this.imageService.imageShareUpDate(
                          oldName,
                          newName,
                          "",
                          "",
                          this.siteType,
                          domain,
                          port
                        )
                      ),
                      concatMap(() =>
                        this.imageService.importSuccess(
                          this.siteType,
                          this.modelName,
                          domain,
                          port,
                          rep[0]
                        )
                      ),
                      catchError(() => {
                        this.toastr.error(
                          this.translate.instant("message.echecI")
                        );
                        return empty();
                      })
                    );
                }
                return empty();
              })
            );
          }
          this.router.navigate(["ecombox/images"]);
          this.toastr.info(
            "L'import" + this.translate.instant("message.attente")
          );
          return this.pullImage(oldName, newName, oldNamedb, newNamedb);
        }),
        catchError(() => {
          this.router.navigate(["ecombox/images"]);
          this.toastr.info(
            "L'import" + this.translate.instant("message.attente")
          );
          return this.pullImage(oldName, newName, oldNamedb, newNamedb);
        })
      )
      .subscribe();
  }

    /**
   * /**
   * @param {string} oldName
   * @param {string} newName
   * @param {string} oldNamedb
   * @param {string} newNamedb
   * @return {Observable<unknown>}
   */
    private pullImage(
      oldName: string,
      newName: string,
      oldNamedb: string,
      newNamedb: string
    ): Observable<unknown> {
      const [domain, port] = this.imageService.urlBasePath();
      return this.imageService.imageShareUpDate(oldNamedb, newNamedb).pipe(
        mergeMap(() => this.imageService.imageShareUpDate(oldName, newName)),
        concatMap(() =>
          this.imageService.importSuccess(
            this.siteType,
            this.modelName,
            domain,
            port,
            true
          )
        ),
        catchError(() => {
          this.toastr.error(this.translate.instant("message.echecI"));
          return empty();
        })
      );
    }
  
  

  /**
   *
   */
  onClose(): void {
    this.router.navigate(["ecombox/images"]);
  }
}
