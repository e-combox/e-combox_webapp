import { RouterModule, Routes } from "@angular/router";
import { NgModule } from '@angular/core';
import { ImagesComponent } from "./images.component";
import { ImageExportFormComponent } from "./image-export-form/image-export-form.component";
import { ImagesImportFormComponent } from "./images-import-form/images-import-form.component";

const routes: Routes = [
  { path: "", component: ImagesComponent },
  { path: "export", component: ImageExportFormComponent },
  { path: "import", component: ImagesImportFormComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImagesRoutingModule { }