import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { EXPORT_SITE } from "../../shared/api/variables";
import { ImageService } from "../../shared/api/image.service";
import { DockerRegistryService } from "../../shared/api/docker-registry.service";
import { StackStore } from "../../shared/store/stack.store";
import { ToastrService } from "ngx-toastr";
import { TranslateService } from "@ngx-translate/core";
import { mergeMap, tap } from "rxjs/operators";
import { empty } from "rxjs";

@Component({
  selector: "ecx-image-export-form",
  templateUrl: "./image-export-form.component.html",
  styleUrls: ["./image-export-form.component.scss"],
})
export class ImageExportFormComponent {
  /**
   * Constructor
   * @param {Router} router
   * @param {StackStore} store
   * @param {DockerRegistryService} registryService
   * @param {ImageService} imageService
   * @param {TranslateService} translate
   */
  constructor(
    private router: Router,
    private store: StackStore,
    private registryService: DockerRegistryService,
    private imageService: ImageService,
    private toastr: ToastrService,
    public translate: TranslateService
  ) {}

  /**
   *
   * @param {string} username
   * @param {string} password
   */
  export(username: string, password: string): void {
    this.toastr.info("L'export" + this.translate.instant("message.attente"));
    const localPort = this.store.getResgistryPort();
    const newName = this.imageService.imageExportName(
      username,
      EXPORT_SITE.siteType,
      EXPORT_SITE.modelName
    );
    const oldName = this.imageService.imageLocalName(
      localPort,
      EXPORT_SITE.siteType,
      EXPORT_SITE.modelName
    );
    const newNamedb = this.imageService.imageExportName(
      username,
      EXPORT_SITE.siteType + this.translate.instant("extension"),
      EXPORT_SITE.modelName
    );
    const oldNamedb = this.imageService.imageLocalName(
      localPort,
      EXPORT_SITE.siteType + this.translate.instant("extension"),
      EXPORT_SITE.modelName
    );

    this.imageService
      .imageShareUpDate(oldNamedb, newNamedb, username, password)
      .pipe(
        mergeMap((res) => {
          if (res.includes("errorDetail")) {
            this.toastr.error(
              this.translate.instant("message.echecE") +
                " Veuillez vérifier votre identifiant et votre mot de passe !"
            );
            return empty();
          } else {
            return this.imageService
              .imageShareUpDate(oldName, newName, username, password)
              .pipe(
                tap((res) => {
                  if (res.includes("errorDetail")) {
                    this.toastr.error(this.translate.instant("message.echecE"));
                  } else {
                    this.toastr.success(
                      this.translate.instant("message.success") + "exporté !"
                    );
                  }
                })
              );
          }
        })
      )
      .subscribe();

    this.router.navigate(["ecombox/images"]);
  }

  /**
   *
   */
  onClose(): void {
    this.router.navigate(["ecombox/images"]);
  }
}
