import { Component, Input } from '@angular/core';
import { NbDialogRef } from "@nebular/theme";

@Component({
  selector: 'ecx-dialog-same-name',
  templateUrl: './dialog-same-name.component.html',
  styleUrls: ['./dialog-same-name.component.scss']
})
export class DialogSameNameComponent {
  @Input() images;
  err: string;

  /**
   * 
   * @param {NbDialogRef} ref 
   */
  constructor(
    protected ref: NbDialogRef<DialogSameNameComponent>,
  ) { }

  /**
   * 
   */
  valid(): void {
    this.ref.close([false, ""]);
  }

  /**
   * 
   * @param {string} name 
   */
  submit(name: string): void {
    if (this.images.tags.includes(name)) {
      this.err = "Un modèle de site du même nom existe aussi. Veuillez choisir un autre nom !";
    } else {
      this.ref.close([true, name]);
    }
  }

  /**
   * 
   */
  cancel(): void {
    this.ref.close([]);
  }
}
