import { NgModule } from "@angular/core";
import { ThemeModule } from "../../@theme/theme.module";
import { TranslateModule } from "@ngx-translate/core";
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbProgressBarModule,
  NbListModule,
  NbIconModule,
  NbTreeGridModule,
  NbInputModule,
  NbSelectModule,
  NbPopoverModule,
} from "@nebular/theme";
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from "@angular/forms";
import { NbDialogModule } from "@nebular/theme";

import { ImagesComponent } from "./images.component";
import { ImageExportFormComponent } from "./image-export-form/image-export-form.component";
import { ImagesRoutingModule } from "./images-routing.module";
import { ImagesImportFormComponent } from "./images-import-form/images-import-form.component";
import { DialogSameNameComponent } from "./dialog-same-name/dialog-same-name.component";
import { Angular2SmartTableModule } from "angular2-smart-table";

@NgModule({
  imports: [
    ThemeModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbProgressBarModule,
    NbListModule,
    NbIconModule,
    NbTreeGridModule,
    NbInputModule,
    ImagesRoutingModule,
    NbSelectModule,
    ReactiveFormsModule,
    FormsModule,
    Angular2SmartTableModule,
    TranslateModule.forChild({
      extend: true,
    }),
    NbDialogModule.forChild(),
    NbPopoverModule,
  ],
  declarations: [
    ImagesComponent,
    ImageExportFormComponent,
    ImagesImportFormComponent,
    DialogSameNameComponent,
  ],
})
export class ImagesModule {}
