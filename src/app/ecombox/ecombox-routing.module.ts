import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { EcomboxComponent } from "./ecombox.component";
import { NotFoundComponent } from "./miscellaneous/not-found/not-found.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { HelpComponent } from "./help/help.component";
import { RedirectGuard } from "./redirect-guard";
import { PORTAINER_URL, RESOURCES_URL } from "./shared/api/variables";
import { ProfileComponent } from "./profile/profile.component";
import { InformationComponent } from "./information/information.component";

const routes: Routes = [
  {
    path: "",
    component: EcomboxComponent,
    children: [
      {
        path: "dashboard",
        component: DashboardComponent,
      },
      {
        path: "images",
        // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
        loadChildren: () =>
          import("./images/images.module").then((mod) => mod.ImagesModule),
      },
      {
        path: "servers",
        // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
        loadChildren: () =>
          import("./servers/servers.module").then((mod) => mod.ServersModule),
      },
      {
        path: "portainer",
        canActivate: [RedirectGuard],
        component: RedirectGuard,
        data: {
          externalUrl: PORTAINER_URL,
        },
      },
      {
        path: "resources",
        canActivate: [RedirectGuard],
        component: RedirectGuard,
        data: {
          externalUrl: RESOURCES_URL,
        },
      },
      {
        path: "help",
        component: HelpComponent,
      },
      {
        path: "profile",
        component: ProfileComponent,
      },
      {
        path: "policies",
        // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
        loadChildren: () =>
          import("./policies/policies.module").then((mod) => mod.PolicyModule),
      },
      {
        path: "information",
        component: InformationComponent,
      },
      {
        path: "**",
        component: NotFoundComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EcomboxRoutingModule {}
