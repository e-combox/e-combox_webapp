import { RouterModule, Routes, ExtraOptions } from "@angular/router";
import { NgModule } from "@angular/core";
import { AuthGuard } from "./auth-guard.service";
import { AuthModule } from "./auth/auth.module";

const config: ExtraOptions = {
  useHash: false,
  relativeLinkResolution: "legacy",
};

const routes: Routes = [
  {
    path: "auth",
    loadChildren: (): Promise<AuthModule> =>
      import("./auth/auth.module").then((m) => m.AuthModule),
  },
  {
    path: "ecombox",
    canActivate: [AuthGuard],
    loadChildren: (): Promise<AuthModule> =>
      import("./ecombox/ecombox.module").then((m) => m.EcomboxModule),
  },
  { path: "", redirectTo: "auth/login", pathMatch: "full" },
  { path: "**", redirectTo: "auth/login" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
