import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { NbAuthService } from "@nebular/auth";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

@Injectable()
export class AuthGuard implements CanActivate {
  /**
   *
   * @param {NbAuthService} authService
   */
  constructor(private authService: NbAuthService, private router: Router) {}
  /**
   *
   * @return {Observable<boolean>}
   */
  canActivate(): Observable<boolean> {
    // we just return the isAuthenticated value,
    // so that when it's true - route can be activated, and vise versa when it's not.
    return this.authService.isAuthenticated().pipe(
      tap((authenticated) => {
        if (!authenticated) {
          this.router.navigate(["auth/login"]);
        }
      })
    );
  }
}
