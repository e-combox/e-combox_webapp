import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { NbAuthService } from "@nebular/auth";
import { Observable } from "rxjs";
import { tap, mergeMap } from "rxjs/operators";
import { ActivatedRouteSnapshot } from "@angular/router";

@Injectable()
export class AuthGuard implements CanActivate {
  /**
   *
   * @param {Router} router
   * @param {NbAuthService} authService
   */
  constructor(private authService: NbAuthService,
    private router: Router) { }
  /**
   *
   * @param {ActivatedRouteSnapshot} route
   * @return {Observable<boolean>}
   */
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    // Authentification avec OAuth0
    if (route.queryParams.code) {
      const code = route.queryParams.code;
      return this.authService.authenticate('email', { code: code }).pipe(
        mergeMap((res) => {
          return this.authService.isAuthenticated().pipe(
            tap((authenticated) => {
              if (!authenticated) {
                this.router.navigate(["auth/login"]);
              }
            })
          );
        })
      );
    }

    // Authentification avec e-comBox
    return this.authService.isAuthenticated().pipe(
      tap((authenticated) => {
        if (!authenticated) {
          this.router.navigate(["auth/login"]);
        }
      })
    );
  }
}
