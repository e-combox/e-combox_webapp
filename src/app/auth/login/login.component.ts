import { AfterViewInit, Component, ViewChild } from "@angular/core";
import { NbLoginComponent } from "@nebular/auth";
import { OAuthComponent } from "../oauth/oauth.component";

@Component({
  selector: "ecx-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent extends NbLoginComponent implements AfterViewInit {
  isOauthEnabled = true;
  // eslint-disable-next-line new-cap
  @ViewChild(OAuthComponent, { static: false }) oauthComponent: OAuthComponent; // Remplacez le chemin par celui de votre composant

  /**
   *
   * @param {boolean} $event
   */
  handleCustomEvent($event: boolean): void {
    this.isOauthEnabled = $event;
  }

  /**
   *
   */
  ngAfterViewInit(): void {
    if (this.oauthComponent) {
      this.oauthComponent.focus();
    }
  }
}
