import { Component, ElementRef, EventEmitter, Output } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API_URL } from "../../ecombox/shared/api/variables";
import { map } from "rxjs/operators";

@Component({
  selector: "ecx-oauth",
  templateUrl: "./oauth.component.html",
  styleUrls: ["./oauth.component.scss"],
})
export class OAuthComponent {
  OAUTH = true;
  oauthUrl: string;
  loginUrl: string;
  // eslint-disable-next-line new-cap
  @Output() onOauthEnabled: EventEmitter<boolean> = new EventEmitter();

  /**
   * @param {HttpClient} httpClient
   */
  constructor(public httpClient: HttpClient, private elementRef: ElementRef) {
    this.httpClient
      .get(`${API_URL}/settings/public`)
      .pipe(
        map((res) => {
          console.log(JSON.stringify(res));
          if (res["OAuthLoginURI"]) {
            this.oauthUrl = res["OAuthLoginURI"];
          } else {
            this.OAUTH = false;
          }
          this.onOauthEnabled.emit(this.OAUTH);
        })
      )
      .subscribe();
  }

  /**
   *
   */
  connect(): void {
    window.location.href = this.oauthUrl;
  }

  /**
   * method to get focus to ecx-oauth component
   */
  focus(): void {
    this.elementRef.nativeElement.focus();
  }
}
